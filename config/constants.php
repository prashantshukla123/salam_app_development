<?php
$pageURL = @$_SERVER["SERVER_NAME"];
$siteFoler = dirname(dirname(@$_SERVER['SCRIPT_NAME']));
$siteUrl = $pageURL.$siteFoler.'/';
$siteUrl = str_replace('\\','/',$siteUrl);
$siteUrl = str_replace('//','/',$siteUrl);

$siteUrl = @$_SERVER['APP_URL'];

return [
	'SITE_URL' =>$siteUrl,
	'SITE_NAME' => 'SALAM',
	'ADMIN_MAIL' => 'salam@mailinator.com',
	'AdminDepartment' => 'SiteControl',
	'ADMIN_PAGE_LIMIT' => '20',
	'ADMIN_PAGE_SHOW' => [1,10,25,50], 
	'formErrorMsg' => 'There are some error in form submission, please find error in below form.',
	'salutation'=>[
		1 => 'Mr',
		2 => 'Mrs',
		3 => 'Miss',
		4 => 'Ms',
		5 => 'Other',
	],
	'allow_img_ext' => [
		'jpg',
		'jpeg',
		'png',
		'gif',
	],
	'allow_img_size' => '512kb',
	'allow_video_size' => (1 * 1024 * 1024)*100,
	'social_handels' => [
		'instagram' => 'Instagram',
		'youtube' => 'YouTube',
		'facebook' => 'Facebook',
		'twitch' => 'Twitch',
		'musically' => 'TikTok',
		'other' => 'Other',
	]
];
?>