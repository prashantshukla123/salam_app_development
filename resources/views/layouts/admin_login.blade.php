<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>{{(isset($pageTitle))?$pageTitle:env('APP_NAME')}}</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="all,follow">
		
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{ asset('css/admin/plugins/fontawesome-free/css/all.min.css') }}">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- icheck bootstrap -->
		<link rel="stylesheet" href="{{ asset('css/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
		<!-- Theme style -->
		<link rel="stylesheet" href="{{ asset('css/admin/dist/adminlte.min.css') }}">
		<!-- Google Font: Source Sans Pro -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
		
		<link rel="stylesheet" href="{{ asset('css/admin/custom.css') }}">
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			@if(Session::has('success'))
				<div class="col-lg-12">
					<div class="alert alert-info alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-info-circle"></i>  <strong>{!! session('success') !!}</strong> 
					</div>
				</div>
			@endif
			
			@if(Session::has('error'))
				<div class="col-lg-12">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-info-circle"></i>  <strong>{!! session('error') !!}</strong> 
					</div>
				</div>
			@endif
			@yield('content')		
		</div>
	</body>

	<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/plugins/bootstrap/bootstrap.bundle.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/dist/adminlte.min.js') }}"></script>
	
	<!-- Jquery validations-->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	
	<script>
		$(function () {
			$.validate({ 
				modules : 'file, sanitize, security',
				inlineErrorMessageCallback: function($input, errorMessage, config) {
					if (errorMessage) {
						if($input.closest('div').hasClass('input-group')){
							$input.closest('div.form-group').find('span.help-block').remove();
							
							$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input.closest('div'));
						}else if($input.closest('div').hasClass('custom-checkbox')){
							$input.closest('div').nextAll().remove();
							
							$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input.closest('div'));
						}else if($input.hasClass('custom-file-input')){
							$input.closest('div').find('span').remove();
							
							$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input.closest('div').find('label'));
						}else{
							$input.nextAll().remove();
							$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input);
						}
					}else {
						if($input.closest('div').hasClass('input-group')){
							$input.closest('div.form-group').find('span.help-block').remove();
						}else{
							$input.nextAll().remove();
						}
					}
					return false; // prevent default behaviour
				},
				submitErrorMessageCallback : function($form, errorMessages, config) {
					/* if (errorMessages) {
						customDisplayErrorMessagesInTopOfForm($form, errorMessages);
					} else {
						customRemoveErrorMessagesInTopOfForm($form);
					}
					return false; // prevent default behaviour */
				}
			});
			
			
			//iCheck for checkbox and radio inputs
			$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				checkboxClass: 'icheckbox_minimal-blue',
				radioClass   : 'iradio_minimal-blue',
				increaseArea: '100%' // optional
			});
		
		});
	</script>
</html>
