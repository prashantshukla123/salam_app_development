<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{(isset($pageTitle))?$pageTitle:env('APP_NAME')}} </title>
	<!--<title>Easy Order Banner</title>-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="all,follow">
	@include('partials.admin.css')
	<script>
		var popupBGcolor = '#000';
		var SITE_NAME = "{{env('APP_NAME')}}";
		var SITE_URL = '{{config('constants.SITE_URL')}}';
		var csrf_token = '{{csrf_token()}}';
	</script>
	<!-- jQuery -->
	<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery/jquery.min.js') }}"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		@include('partials.admin.header')
		@include('partials.admin.navAdmin')
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			@if(Session::has('success'))
				<div class="col-lg-12">
					<div class="alert alert-success alert-dismissable" data-dismiss="alert" aria-hidden="true">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-info-circle"></i>  <strong>{!! session('success') !!}</strong> 
					</div>
				</div>
				
				<script>
					setTimeout(function(){ $('.close').trigger('click'); },5000);
				</script>
			@endif
			
			@if(Session::has('error'))
				<div class="col-lg-12">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-info-circle"></i>  <strong>{!! session('error') !!}</strong> 
					</div>
				</div>
				
				<script>
					setTimeout(function(){ $('.close').trigger('click'); },5000);
				</script>
			@endif
			
			@yield('content')
		</div>
		<div class="control-sidebar-bg"></div>
		<!-- Main Footer -->
		@include('partials.admin.footer')
	</div>
	@include('partials.admin.javascripts')
	
</body>
</html>
