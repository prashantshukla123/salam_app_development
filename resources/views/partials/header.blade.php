<header class="navigation fixed-top">
	<div class="login_user_block p-2">
		<div class="container">
			<ul class="top_login_user ml-auto">
				<li><a href="{{url('/dashboard')}}">Welcome {{\Auth::user()->fname}}! 
				@if(@getimagesize(url('/uploads/users/'.Auth::user()->image)))
					<img class="user_img" src="{{URL::to('/uploads/users/'.Auth::user()->image)}}" alt="Login User">
				@else
					<img class="user_img" src="{{asset('img/front/profile_img.png')}}" alt="Login User" />
				@endif
				</a></li>
			</ul>
		</div>
	</div>
	@include('partials.nav')
</header>
<!-- Below Code for show success and error message  -->
@if(Session::has('success') || Session::has('error'))						
	@if(Session::has('success'))										
		<script>
			$(document).ready(function() { 
				toastr.success('<strong>{!! session("success") !!}</strong>');
			}); 
		</script>
	@endif

	@if(Session::has('error'))
		<script>
			$(document).ready(function() { 
				toastr.error('<strong>{!! session("error") !!}</strong>');
			}); 
		</script>
	@endif
@endif
<!-- End Code for show success and error message  -->