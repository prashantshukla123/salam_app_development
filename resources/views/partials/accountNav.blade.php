@php
$currentAction = \Route::currentRouteAction();		
list($controller, $action) = explode('@', $currentAction);
$controller = preg_replace('/.*\\\/', '', $controller);

$designation = \DB::table('designations')->where('id',Auth::user()->designation)->select('name')->first();
$department = \DB::table('departments')->where('id',Auth::user()->designation)->select('name')->first();

@endphp
<div class="left_sidebar">
	<div class="frofile_user">
		<div class="frofile_img">
			@if(@getimagesize(url('/uploads/users/'.Auth::user()->image)))
				<img class="" src="{{URL::to('/uploads/users/'.Auth::user()->image)}}" alt="Photo">
			@else
				<img src="{{asset('img/front/profile_img.png')}}" alt="" />
			@endif
			<a href="{{url('my-account-avatar')}}"><img src="{{asset('img/front/camera_icon.png')}}" alt="" /> </a>
		</div>
		<h4>{{Auth::user()->fname.' '.Auth::user()->lname}}</h4>
		<p>
			{{(!empty($designation))?$designation->name:''}} 
			@if(!empty($department))
			(<b>{{$department->name}}</b>)
			@endif
		</p>
	</div>
	<div class="left_nav sidebarnav navbar-expand-md">	
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="margin-left:5px;">
			<span class="navbar-toggler-icon"></span>
			<span class="navbar-toggler-icon"></span>
			<span class="navbar-toggler-icon"></span>
		</button>
		
		<ul  class="collapse navbar-collapse " id="sidebarbarCollapse">
			<li class="{{($controller=='HomeController' and $action == 'myAccount')?'active':''}}">
				<a href="{{url('dashboard')}}" class="home"> Dashboard</a>
			</li>
			
			<li>
				<a href="{{url('my-account')}}" class="icon_setting">My Account</a>
			</li>
			
			<!--<li>
				<a href="#" class="icon_mylevels"> My Levels</a>
			</li>-->

			<li>
				<a href="{{url('/my-score')}}" class="icon_myscores"> My Scores</a>
			</li>

			<li>
				<a href="{{url('/badges')}}" class="icon_mybadges"> My Badges</a>
			</li>

			<li>
				<a href="{{url('/settings')}}" class="icon_setting"> Settings </a>
			</li>

			<!--<li>
				<a href="#" class="icon_notifications"> Notifications</a>
				<span class="notifications">03</span>
			</li>-->
			
			<li>
				<a href="{{url('/logout')}}" class="icon_logout"> Logout</a>
			</li>
		</ul>
	</div>
</div>