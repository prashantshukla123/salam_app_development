@php
$currentAction = \Route::currentRouteAction();		
list($controller, $action) = explode('@', $currentAction);
$controller = preg_replace('/.*\\\/', '', $controller);
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="{{url('sitecontrol/dashboard')}}" class="brand-link">
		@if(!empty(options['logo']) && @getimagesize(public_path('/uploads/logo/'.options['logo'])) && @file_exists(public_path('/uploads/logo/'.options['logo'])))
			<img src="{{asset('/uploads/logo/'.options['logo'])}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8" />
		@else
			<span class="brand-text font-weight-light">
				{{ (!empty(options['site_name'])) ? options['site_name'] : env('APP_NAME') }}
			</span>
		@endif
    </a>
	<div class="sidebar">
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				@php
				$class="";
				if($controller == 'HomeController' and $action=='index')
					$class = 'active';
				@endphp
				<li class="nav-item">
					<a class="nav-link {{$class}}" href="{{url('sitecontrol/dashboard')}}"><i class="nav-icon fas fa-tachometer-alt"></i> <span>Dashboard</span></a>
				</li>
				
				@php
				$class="";$active='';
				if(in_array($controller, ['UserController','ArtistsController']) and in_array($action,array('lists','addForm','edit','view'))){
					$class = 'menu-open';$active = 'active';
				}
				@endphp
				<li class="nav-item has-treeview {{$class}}">
					<a href="#" class="nav-link {{$active}}">
						<i class="nav-icon fas fa-user"></i>
						<p>User Manager<i class="right fas fa-angle-left"></i></p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a class="nav-link @php if($controller=='UserController' && $action=='lists'){ echo 'active';}@endphp" href="{{ url('sitecontrol/users/lists') }}"><i class="far fa-circle nav-icon"></i> List Users</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @php if($controller=='UserController' && $action=='addForm'){ echo 'active';}@endphp" href="{{ url('sitecontrol/users/add') }}"><i class="far fa-circle nav-icon"></i> Add Fan</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @php if($controller=='ArtistsController' && $action=='addForm'){ echo 'active';}@endphp" href="{{ url('sitecontrol/artists/add') }}"><i class="far fa-circle nav-icon"></i> Add Artist</a>
						</li>
					</ul>
				</li>
				
				@php
				$class="";$active='';
				if($controller == 'ArtistsController' and in_array($action,array('lists','addForm','edit','view'))){
					$class = 'menu-open';$active = 'active';
				}
				@endphp
				<li class="nav-item has-treeview d-none {{$class}}">
					<a href="#" class="nav-link {{$active}}">
						<i class="nav-icon fas fa-user"></i>
						<p>Artists Manager<i class="right fas fa-angle-left"></i></p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a class="nav-link @php if($controller=='ArtistsController' && $action=='lists'){ echo 'active';}@endphp" href="{{ url('sitecontrol/artists/lists') }}"><i class="far fa-circle nav-icon"></i> List Artists</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @php if($controller=='ArtistsController' && $action=='addForm'){ echo 'active';}@endphp" href="{{ url('sitecontrol/artists/add') }}"><i class="far fa-circle nav-icon"></i> Add Artist</a>
						</li>
					</ul>
				</li>
				
				@php
				$class="";$active='';
				if($controller == 'CategoriesController' and in_array($action,array('lists','addForm','edit','view'))){
					$class = 'menu-open';$active = 'active';
				}
				@endphp
				<li class="nav-item has-treeview {{$class}}">
					<a href="#" class="nav-link {{$active}}">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<p>Categories Manager<i class="right fas fa-angle-left"></i></p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a class="nav-link @php if($controller=='CategoriesController' && $action=='lists'){ echo 'active';}@endphp" href="{{ url('sitecontrol/categories/lists') }}"><i class="far fa-circle nav-icon"></i> List categories</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @php if($controller=='CategoriesController' && $action=='addForm'){ echo 'active';}@endphp" href="{{ url('sitecontrol/categories/add') }}"><i class="far fa-circle nav-icon"></i> Add category</a>
						</li>
					</ul>
				</li>
				
				@php
				$class="";$active='';
				if(($controller == 'EmailsController' || $controller == 'PagesController') and in_array($action,array('lists','addForm','edit','view'))){
					$class = 'menu-open';$active='active';
				}
				@endphp
				<li class="nav-item has-treeview {{$class}}">
					<a href="#" class="nav-link {{$active}}">
						<i class="fa fa-square"></i>
						<p>Content Manager<i class="right fas fa-angle-left"></i></p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a class="nav-link @php if($controller=='EmailsController' && $action=='lists'){ echo 'active';}@endphp" href="{{ url('sitecontrol/emails/lists') }}">
								<i class="fa fa-envelope" aria-hidden="true"></i>
								Email Temlpates
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @php if($controller=='PagesController' && $action=='lists'){ echo 'active';}@endphp" href="{{ url('sitecontrol/pages/lists') }}">
								<i class="fa fa-envelope" aria-hidden="true"></i>
								Static Pages
							</a>
						</li>
					</ul>
				</li>
				
				<li class="nav-header">Settings</li>
				@php
				$class="";$active='';
				if($controller == 'SettingsController' and in_array($action,array('admin'))){
					$class = 'menu-open';$active='active';
				}
				@endphp
				<li class="nav-item">
					<a href="{{ url('sitecontrol/setting/admin') }}" class="nav-link {{$active}}">
						<i class="nav-icon far fa-circle text-danger"></i> <p>Admin Settings</p>
					</a>
				</li>
			</ul>
		</nav>
	</div>
</aside>