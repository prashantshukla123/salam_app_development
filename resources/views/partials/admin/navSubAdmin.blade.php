@php
$currentAction = \Route::currentRouteAction();		
list($controller, $action) = explode('@', $currentAction);
$controller = preg_replace('/.*\\\/', '', $controller);

@endphp
<aside class="main-sidebar">
	<section class="sidebar">			
		<ul class="sidebar-menu" data-widget="tree">
			@php
				$class="";
				if($controller == 'HomeController' and $action=='index')
					$class = 'active';
			@endphp
			<li class="{{$class}}">
				<a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
			</li>
			@php
			if(array_key_exists('SubAdminsController',permission) && (in_array('addForm',permission['SubAdminsController']) || in_array('lists',permission['SubAdminsController']))){
			$class="";
				if($controller == 'SubAdminsController' && in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-address-book"></i>
					<span>Sub Admin Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('addForm',permission['SubAdminsController']))
					<li class="@php if($controller=='SubAdminsController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/sub-admin/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['SubAdminsController']))
					<li class="@php if($controller=='SubAdminsController' && $action=='addForm'){ echo 'active';}@endphp">
						<a href="{{ url('admin/sub-admin/add') }}"><i class="fa fa-plus-square-o"></i> Add</a>
					</li>
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('UserController',permission) && (in_array('addForm',permission['UserController']) || in_array('lists',permission['UserController']))){
			$class="";
			if($controller == 'UserController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-user"></i>
					<span>Users Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['UserController']))
					<li class="@php if($controller=='UserController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/users/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('addForm',permission['UserController']))
					<li class="@php if($controller=='UserController' && $action=='addForm'){ echo 'active';}@endphp">
						<a href="{{ url('admin/users/add') }}"><i class="fa fa-plus-square-o"></i> Add</a>
					</li>
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('CampaignsController',permission) && (in_array('addForm',permission['CampaignsController']) || in_array('lists',permission['CampaignsController']))){
			$class="";
			if($controller == 'CampaignsController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-asterisk"></i>
					<span>Industry/Campaign Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['CampaignsController']))
					<li class="@php if($controller=='CampaignsController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/campaign/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('addForm',permission['CampaignsController']))
					<!--<li class="@php if($controller=='CampaignsController' && $action=='addForm'){ echo 'active';}@endphp">
						<a href="{{ url('admin/campaign/add') }}"><i class="fa fa-plus-square-o"></i> Add</a>
					</li>-->
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('CompaniesController',permission) && (in_array('addForm',permission['CompaniesController']) || in_array('lists',permission['CompaniesController']))){
			$class="";
			if($controller == 'CompaniesController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-building-o"></i>
					<span>Company/Buyer Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['CompaniesController']))
					<li class="@php if($controller=='CompaniesController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/company/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['CompaniesController']))
					<li class="@php if($controller=='CompaniesController' && $action=='addForm'){ echo 'active';}@endphp">
						<a href="{{ url('admin/company/add') }}"><i class="fa fa-plus-square-o"></i> Add</a>
					</li>
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('QuestionsController',permission) && (in_array('addForm',permission['QuestionsController']) || in_array('lists',permission['QuestionsController']))){
			$class="";
			if($controller == 'QuestionsController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-lg fa-list-alt"></i>
					<span>Question Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['QuestionsController']))
					<li class="@php if($controller=='QuestionsController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/question/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['QuestionsController']))
					<li class="@php if($controller=='QuestionsController' && $action=='addForm'){ echo 'active';}@endphp">
						<a href="{{ url('admin/question/add') }}"><i class="fa fa-plus-square-o"></i> Add</a>
					</li>
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('CheatSheetsController',permission) && (in_array('addForm',permission['CheatSheetsController']) || in_array('lists',permission['CheatSheetsController']))){
			$class="";
			if($controller == 'CheatSheetsController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-file-text"></i>
					<span>Cheat Sheet Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['CheatSheetsController']))
					<li class="@php if($controller=='CheatSheetsController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/cheat-sheet/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['CheatSheetsController']))
					<li class="@php if($controller=='CheatSheetsController' && $action=='addForm'){ echo 'active';}@endphp"><a href="{{ url('admin/cheat-sheet/add') }}"><i class="fa fa-plus-square-o"></i> Add</a></li>
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('StaticpagesController',permission) && (in_array('addForm',permission['StaticpagesController']) || in_array('lists',permission['StaticpagesController']))){
			$class="";
			if($controller == 'StaticpagesController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-file-text"></i>
					<span>Static Pages</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['StaticpagesController']))
					<li class="@php if($controller=='StaticpagesController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/staticpages/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['StaticpagesController']))
					<!--<li class="@php if($controller=='StaticpagesController' && $action=='addForm'){ echo 'active';}@endphp"><a href="{{ url('admin/staticpages/add') }}"><i class="fa fa-plus-square-o"></i> Add</a></li>-->
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('EmailsController',permission) && (in_array('addForm',permission['EmailsController']) || in_array('lists',permission['EmailsController']))){
			$class="";
			if($controller == 'EmailsController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-envelope-o"></i>
					<span>Emails Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['EmailsController']))
					<li class="@php if($controller=='EmailsController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/emails/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['EmailsController']))
					<!--<li class="@php if($controller=='EmailsController' && $action=='addForm'){ echo 'active';}@endphp"><a href="{{ url('admin/emails/add') }}"><i class="fa fa-plus-square-o"></i> Add</a></li>-->
					@endif
				</ul>
			</li>									
			@php
			}
			
			if(array_key_exists('VideosController',permission) && (in_array('addForm',permission['VideosController']) || in_array('lists',permission['VideosController']))){
			$class="";
			if($controller == 'VideosController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-video-camera"></i>
					<span>Video Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['VideosController']))
					<li class="@php if($controller=='VideosController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/video/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['VideosController']))
					<li class="@php if($controller=='VideosController' && $action=='addForm'){ echo 'active';}@endphp"><a href="{{ url('admin/video/add') }}"><i class="fa fa-plus-square-o"></i> Add</a></li>
					@endif
				</ul>
			</li>
			@php
			}
			
			if(array_key_exists('BadgesController',permission) && (in_array('addForm',permission['BadgesController']) || in_array('lists',permission['BadgesController']))){
			$class="";
			if($controller == 'BadgesController' and in_array($action,array('lists','addForm','edit','view')))
				$class = 'active menu-open';
			@endphp
			<li class="treeview {{$class}}">
				<a href="#">
					<i class="fa fa-id-badge"></i>
					<span>Badges Manager</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				</a>
				<ul class="treeview-menu">
					@if(in_array('lists',permission['BadgesController']))
					<li class="@php if($controller=='BadgesController' && $action=='lists'){ echo 'active';}@endphp">
						<a href="{{ url('admin/badge/lists') }}"><i class="fa fa-list"></i> List</a>
					</li>
					@endif
					@if(in_array('lists',permission['BadgesController']))
					<!--<li class="@php if($controller=='BadgesController' && $action=='addForm'){ echo 'active';}@endphp"><a href="{{ url('admin/badge/add') }}"><i class="fa fa-plus-square-o"></i> Add</a></li>-->
					@endif
				</ul>
			</li>
			@php
			}
			@endphp
		</ul>
	</section>
</aside>