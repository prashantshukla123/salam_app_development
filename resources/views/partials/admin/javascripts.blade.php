<!-- REQUIRED SCRIPTS -->

<!-- Bootstrap -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/bootstrap/bootstrap.bundle.min.js') }}"></script>

<!-- overlayScrollbars -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/overlayScrollbars/jquery.overlayScrollbars.min.js') }}"></script>

<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('js/admin/dist/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script type="text/javascript" src="{{ asset('js/admin/dist/demo.js') }}"></script>

<!-- PAGE PLUGINS -->

<!-- jQuery Mapael -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>

@php
	$currentAction = \Route::currentRouteAction();		
	list($controller, $action) = explode('@', $currentAction);
	$controller = preg_replace('/.*\\\/', '', $controller);
@endphp

@if($controller == 'HomeController' and $action=='index')
	<script type="text/javascript" src="{{ asset('js/admin/plugins/raphael/raphael.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

	<!-- ChartJS -->
	<script type="text/javascript" src="{{ asset('js/admin/plugins/chart.js/Chart.min.js') }}"></script>
	<!-- PAGE SCRIPTS -->
	<script type="text/javascript" src="{{ asset('js/admin/dist/dashboard2.js') }}"></script>
@endif

@stack('scripts')

<!-- blockUI for show loader on ajax call -->
<script src="{{ asset('js/admin/jquery.blockUI.js') }}"></script>

<!-- Jquery validations-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<!-- Ckeditor SCRIPTS -->
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

<!-- CAMEO CUSTOM SCRIPTS -->
<script src="{{ asset('js/admin/common.js') }}"></script>