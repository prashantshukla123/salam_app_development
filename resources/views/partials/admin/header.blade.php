<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>
		<li class="nav-item d-none d-sm-inline-block">
			<a href="{{url('sitecontrol/dashboard')}}" class="nav-link">Home</a>
		</li>
	</ul>
	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">
		<!-- Messages Dropdown Menu -->
		<li class="nav-item dropdown">
			<a class="nav-link" data-toggle="dropdown" href="#">
				<span class="hidden-xs">{{\Auth::user()->name}}</span>
			</a>
			<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
				<div class="card m-0">
					<div class="card-body text-center">
						<a href="{{ url('/sitecontrol/profile') }}" class="btn btn-default d-block">Edit Profile</a>
						<div class="dropdown-divider my-2"></div>
						<a href="{{ url('/sitecontrol/change-password') }}" class="btn btn-default d-block">Change Password</a>
						<div class="dropdown-divider my-2"></div>
						<a href="{{ url('/sitecontrol/logout') }}" class="btn btn-default d-block">Sign out</a>
					</div>
				</div>
				<?php /*
				<ul>
					<!-- User image -->
					<!--<li class="user-header">
						@if(@getimagesize(url('/uploads/users/'.\Auth::user()->image)))
							<img src="{{URL::to('/uploads/users/'.\Auth::user()->image)}}" class="img-circle" alt="Photo">
						@else
							<img src="{{asset('img/admin/avatar-1.jpg')}}" class="img-circle" alt="User Image">
						@endif
						<p>
							{{\Auth::user()->fname.' '.\Auth::user()->lname}}
							<small>{{\Auth::user()->email}}</small>
						</p>
					</li>-->
					<li class="dropdown-divider"></li>
					<!-- Menu Body -->
					<!--<li class="user-body">
						<div class="row">
							<div class="col-xs-6 text-center">
								<a href="{{ url('/sitecontrol/change-password') }}" class="btn btn-primary btn-flat">Change Password</a>
							</div>
							<div class="col-xs-6 text-center">
								<a href="{{ url('/sitecontrol/profile') }}" class="btn btn-default btn-warning">Profile</a>
							</div>
						</div>
						
					</li>
					<li class="dropdown-divider"></li>
					
					<li class="user-footer">
						<div class="pull-right">
							<a href="{{ url('/sitecontrol/logout') }}" class="btn btn-success btn-flat">Sign out</a>
						</div>
					</li>-->
				</ul>
				*/?>
			</div>
		</li>
	  <!-- Notifications Dropdown Menu -->
	</ul>
</nav>
<!-- /.navbar -->