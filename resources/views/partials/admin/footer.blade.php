<footer class="main-footer">
	<strong>Copyright &copy; {{date('Y')}} {{env('APP_NAME')}}.</strong>
	All rights reserved.
</footer>


<!--++++ Record Delete Alert Message ++++-->
<div class="modal fade" id="deleteBox">
	<div class="modal-dialog">
		{{Form::model('DeleteForm',['url'=>'','action'=>'post','role'=>'form', 'id' => 'RecordDeleteForm'])}}
		<div class="modal-content">
			{{ Form::hidden('id', '', ['id'=>'recordDeleteID']) }}
			<div class="modal-body">
				<p>Are you sure, you would like to delete this record?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				{{Form::submit('Delete',['class'=>'btn btn-primary'])}}
			</div>
		</div>
		{{Form::close()}}
	</div>
</div>
<!--++++ End Record Delete Alert Message ++++-->

<!--++++ Record Change status Alert Message ++++-->
<div class="modal fade" id="StatusBox">
	<div class="modal-dialog">
		{{Form::model('ChangeStatusForm',['url'=>'','action'=>'post','role'=>'form', 'id' => 'RecordStatusFormId'])}}
		<div class="modal-content">
			{{ Form::hidden('id', '', ['id'=>'recordID']) }}
			{{ Form::hidden('status', '', ['id'=>'recordStatus']) }}
			<div class="modal-body">
				<p>You are about to <span id="statusname"></span> this record?<span id="archMsg"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				{{Form::submit('Yes',['class'=>'btn btn-primary'])}}
			</div>
		</div>
		{{Form::close()}}
	</div>
</div>
<!--++++ End Record Change status Alert Message ++++-->