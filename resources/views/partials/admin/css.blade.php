<!-- Font Awesome Icons -->
<link rel="stylesheet" href="{{ asset('css/admin/plugins/fontawesome-free/css/all.min.css') }}">

<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{ asset('css/admin/plugins/overlayScrollbars/OverlayScrollbars.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('css/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('css/admin/dist/adminlte.min.css') }}">

<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

@stack('stylesheets')
<!-- Camep Custom style -->
<link href="{{ asset('css/admin/custom.css') }}" rel="stylesheet">