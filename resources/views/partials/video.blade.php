	
<!-- Modal -->
    <div class="modal fade" id="VideoPlayer" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered videoPopup" role="document">
            <div class="modal-content p-0 rounded-0">
				<div class="playerClose">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
                <div class="modal-body p-0 rounded-0 VideoFrame" id="VideoFrame"></div>
				<div class="modal-footer video_footer">
					<div class="control_outer">
						<div class="left-play">
							<i data="1" class="material-icons playPause pointer">pause</i>
						</div>
						
						<div class="controlbox pr-4 pl-2" style="padding-top:16px;">
							<input class="videoSlider slideChange" type="range" id="progress-bar" min="0" max="100" value="0"/>
						</div>
						
						<div class="right-valu">
							<i id="mute-toggle" class="material-icons pointer">volume_up</i> 
							<input id="volume-input" type="range" min="1" max="100" value="20" class="slider slideChange" />
						</div>
				
						<div class="right-time">
							<span id="current-time">0:00</span> / <span id="duration">0:00</span>
							<i id="fullScreen" class="material-icons pointer" style="position: relative;top:7px;">fullscreen</i>
						</div>
						<div class="clearfix"></div>
					</div>
				 
				</div>
            </div>
        </div>
    </div>
<!-- End Moda -->

<script src="https://www.youtube.com/iframe_api"></script>

<script>
	var player,iframe,time_update_interval = 0,currentPlayVideoId=0;
	var obj = document.querySelector.bind(document);
	
	function onYouTubeIframeAPIReady() {
		player = new YT.Player('VideoFrame', {
			width: '100%',
			height: '600px',
			playerVars: {
				color: 'white',
				controls: 0,
				showinfo: 0,
				rel: 0,
				nologo:1,
			},
			events: {
				onReady: initialize,
				onStateChange: onPlayerStateChange
			}
		});
		
		
	}
	
	function initialize(event){
		// Update the controls on load
		updateTimerDisplay();
		
		// Clear any old interval.
		clearInterval(time_update_interval);
		
		player = event.target;
		iframe = obj('#VideoFrame');
		setupListener();
		
		//check = 0;
		setTimeInterval();
		
	}
	
	function setTimeInterval(){
		time_update_interval = setInterval(function () {
			updateTimerDisplay();
			updateProgressBar();
			slideValue();
		}, 1000);
	}
	
	function onPlayerStateChange(event) {
		if (event.data == YT.PlayerState.PLAYING) {
			$('.playPause').attr('data',1);
			$('.playPause').html('pause');
			player.playVideo();
			
			if ( typeof audio !== 'undefined' && audio != null) {
				audio.pause();
			}			
		}else if (event.data == YT.PlayerState.PAUSED) {
			$('.playPause').attr('data',0);
			$('.playPause').html('play_arrow');
			player.pauseVideo();
			
			if ( typeof audio !== 'undefined' && audio != null) {
				audio.play();
			}
		}else if (event.data == YT.PlayerState.ENDED) {
			$('.playPause').attr('data',0);
			$('.playPause').html('play_arrow');
			
			if (typeof saveAnswer === "function"){
				var data = saveAnswer();
				if(data.status == 'success'){
					@if(isset($level))
						window.location.href = '{{url("/result/board/")}}'+'/{{$level}}'+'/'+data.earned_points;
					@endif
				}
			}
			
			if (typeof tabType === "string" && tabType == 'video'){
				$.ajax({
					url:'{{url("/ajax/watch-video")}}',
					type:'post',
					dataType:'json',
					async: false,
					headers: {'X-CSRF-TOKEN': csrf_token},
					data:{'video_id':currentPlayVideoId},
					success:function(data){
						if(data.status == 1){
							$("#VideoPlayer").modal('hide');
						}
					}
				});
			}
			
			if ( typeof audio !== 'undefined' && audio != null) {
				audio.play();
			}
		}
	}
	
	// This function is called by initialize()
	function updateProgressBar(){
		// Update the value of our progress bar accordingly.
		if(player.getDuration()>0)
			$('#progress-bar').val((player.getCurrentTime() / player.getDuration()) * 100);
	}
	
	// Progress bar

	$('#progress-bar').on('mouseup touchend', function (e) {

		// Calculate the new time for the video.
		// new time in seconds = total duration in seconds * ( value of range input / 100 )
		var newTime = player.getDuration() * (e.target.value / 100);

		// Skip video to new time.
		player.seekTo(newTime);

	});

	$('.video_block').on('click', function () {
		var url = $(this).attr('data-id');
		player.cueVideoById(url);
		
		$("#VideoPlayer").modal('show');
		
		currentPlayVideoId = Number($(this).attr('data'));
		
		//check = 0;
		
		player.playVideo();
	});
	
	if($('.loadVideo').is(':visible')){
		setTimeout(function(){
			var url = $('.loadVideo').attr('data-id');
			player.cueVideoById(url);
		},2000);
	}

	function playVideo(url){
		setTimeout(function(){
			player.cueVideoById(url);
		},1000);
	}

	$("#VideoPlayer").on('hide.bs.modal', function(){
		player.stopVideo();
    });
    
	// Playback
	$(document).on('click','.playPause', function () {
		var val = $(this).attr('data');
		if(val == 1){
			$(this).attr('data',0);
			$(this).html('play_arrow');
			player.pauseVideo();
		}else if(val == 0){
			$(this).attr('data',1);
			$(this).html('pause');
			player.playVideo();
		}
		
	});
	
	// Below code for video pause/play when user change tab of browser
	document.addEventListener("visibilitychange", function() {
		if (document.hidden){
			player.pauseVideo();
		} else {
			player.playVideo();
		}
	});
	
	// Sound volume
	$('#mute-toggle').on('click', function() {
		var mute_toggle = $(this);

		if(player.isMuted()){
			player.unMute();
			mute_toggle.text('volume_up');
		}
		else{
			player.mute();
			mute_toggle.text('volume_off');
		}
	});

	$('#volume-input').on('change', function () {
		player.setVolume($(this).val());
	});
	
	// This function is called by initialize()
	function updateTimerDisplay(){
		if(typeof pauseAction !== 'undefined' && (Math.round(player.getCurrentTime()) == Number(pauseTime)) && Object.keys(questions).length > 0){
			player.pauseVideo();
			
			clearInterval(time_update_interval);
		
			var html = getQuestion();
			
			if(IsFullScreenCurrently())
				GoOutFullscreen();
			
			$('#questionModel .modal-content').html(html);
			$('#questionModel').modal('show');			
		}
		
		// Update current time text display.
		$('#current-time').text(formatTime( player.getCurrentTime() ));
		$('#duration').text(formatTime( player.getDuration() ));
	}
	
	// Below Code for set full-screen code
	function setupListener (){
		$('#fullScreen').bind('click', playFullscreen);
	}

	function playFullscreen (){
		var requestFullScreen = iframe.requestFullScreen || iframe.mozRequestFullScreen || iframe.webkitRequestFullScreen;
		//console.log(iframe);
		if (requestFullScreen) {
			requestFullScreen.bind(iframe)();
		}
	};
	
	// End Code for set full-screen code
	
	// Helper Functions
	function formatTime(time){
		time = Math.round(time);
		var minutes = Math.floor(time / 60),
		seconds = time - minutes * 60;
		seconds = seconds < 10 ? '0' + seconds : seconds;
		return minutes + ":" + seconds;
	}
	
	function hmsToSecondsOnly(str) {
		var p = str.split(':'),
			s = 0, m = 1;

		while (p.length > 0) {
			s += m * parseInt(p.pop(), 10);
			m *= 60;
		}

		return s;
	}
	
	// End Helper Functions
	
	var slider = document.getElementById("volume-input");
	
	// Video slider bar change functions //
	
	$('input[type="range"]').change(function () {
		var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
		$(this).css('background-image','-webkit-gradient(linear, left top, right top, '+ 'color-stop(' + val + ', #f84e03), '+ 'color-stop(' + val + ', #d3d3d3)'+ ')');
	});
	
	function slideValue(){
		$('.slideChange').each(function(){
			var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
			$(this).css('background-image','-webkit-gradient(linear, left top, right top, '+ 'color-stop(' + val + ', #f84e03), '+ 'color-stop(' + val + ', #d3d3d3)'+ ')');
		});
	}
	
		
	/* Below Code for video full screen */
	
	/* Get into full screen */
	function GoInFullscreen(element) {
		if(element.requestFullscreen)
			element.requestFullscreen();
		else if(element.mozRequestFullScreen)
			element.mozRequestFullScreen();
		else if(element.webkitRequestFullscreen)
			element.webkitRequestFullscreen();
		else if(element.msRequestFullscreen)
			element.msRequestFullscreen();
	}

	/* Get out of full screen */
	function GoOutFullscreen() {
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}

	/* Is currently in full screen or not */
	function IsFullScreenCurrently() {
		var full_screen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;
		
		// If no element is in full-screen
		if(full_screen_element === null)
			return false;
		else
			return true;
	}

	$("#go-button").on('click', function() {
		if(IsFullScreenCurrently())
			GoOutFullscreen();
		else
			GoInFullscreen($("#element").get(0));
	});

	$(document).on('fullscreenchange webkitfullscreenchange mozfullscreenchange MSFullscreenChange', function() {
		if(IsFullScreenCurrently()) {
			$("#element object").css('height','100%');
		}
		else {
			$("#element object").css('height','400px');
		}
	});
</script>