<!-- Optional JavaScript -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700">

<!-- jQuery first, then Popper.js, then Bootstrap JS -->

@if(isset($slug) && ($slug == 'mapTEST' || $slug == 'map'))
	<script type="text/javascript" src="{{ asset('js/front/jquery.js') }}"></script>
@else
	<script type="text/javascript" src="{{ asset('js/front/jquery.min.js') }}"></script>
@endif


<script type="text/javascript" src="{{ asset('js/front/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/front/bootstrap.min.js') }}"></script>

<!-- blockUI for show loader on ajax call -->
<script src="{{ asset('js/admin/jquery.blockUI.js') }}"></script>

<!-- Toastr JS-->
<script src="{{ asset('js/front/toastr.min.js') }}"></script>

<!-- Jquery validations-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script> 

<script src="{{ asset('js/front/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('js/front/common.js') }}"></script>

<script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('js/front/clickBubble.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>