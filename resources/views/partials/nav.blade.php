<nav class="navbar navbar-expand-md">
	<div class="container position-relative">
		<a class="navbar-brand" href="{{url('/')}}" title="Data Hero"><img src="{{asset('/uploads/logo/'.options['logo'])}}" alt="Data Hero"></a>
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			<span class="navbar-toggler-icon"></span>
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item {{(isset($slug) && $slug == 'about-us')?'active':''}}"><a href="{{url('/about-us')}}" title="About Guardium"><span>About Guardium</span></a></li>
				
				<li class="nav-item {{(isset($slug) && $slug == 'videos')?'active':''}}"><a href="{{url('/videos')}}" title="Videos"><span>Videos</span></a></li>
				
				<li class="nav-item {{(isset($slug) && $slug == 'rules-and-faq')?'active':''}}"><a href="{{url('/rules-and-faq')}}" title="Rules &amp; FAQ"><span>Rules &amp; FAQ</span></a></li>
				
				<li class="nav-item {{(isset($slug) && $slug == 'leaderboard')?'active':''}}"><a href="{{url('leaderboard')}}" title="Leaderboard"><span>Leaderboard</span></a></li>
				
				<li class="nav-item {{(isset($slug) && $slug == 'map')?'active':''}}"><a href="{{url('/map')}}" title="Map"><span>Map</span></a></li>
				
				<li class="nav-item {{(isset($slug) && $slug == 'game-play')?'active':''}}"><a href="{{url('game-play')}}" title="Gameplay"><span>Gameplay</span></a></li>
			</ul>
		</div>
	</div>
</nav>