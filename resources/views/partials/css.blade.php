<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link href="{{ asset('css/front/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link href="{{ asset('css/front/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/front/media.css') }}" rel="stylesheet">

<!----- glyphicon ------->
<link href="{{ asset('css/front/glyphicon.css') }}" rel="stylesheet">

<!--<link href="{{ asset('css/front/table_scroll.css') }}" rel="stylesheet">-->

<!-- Toastr CSS-->
<link href="{{ asset('css/front/toastr.min.css') }}" rel="stylesheet">

<!-- Custom Theme files -->
<link href="{{ asset('css/front/devloper.css') }}" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link href="{{ asset('css/admin/bootstrap-datepicker.min.css') }}" rel="stylesheet">
