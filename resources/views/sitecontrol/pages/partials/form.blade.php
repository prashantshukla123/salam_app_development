{{Form::model($page,['url'=>url('sitecontrol/pages/save/'.$page->id),'action'=>'post','role'=>'form','class'=>'form-horizontal form-bordered'])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		if(isset($page->id)){
			$requiredAsterisk = '';
			echo Form::hidden('id',$page->id);
		}
	@endphp
	<div class="row">
		<div class="col-md-9 col-sm-12 pr-4 d-border-right">
			<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('title', 'Title',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('title',null,['class'=>'form-control','placeholder'=>'Title','data-validation'=>'required length', 'data-validation-length' => '1-255'])}}
						@if ($errors->has('title'))
							<span class="help-block">{{ $errors->first('title') }}</span>
						@endif
					</div>
				</div>
			</div>
			
			<div class="form-group {{ $errors->has('meta_title') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('meta_title', 'Meta Title',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('meta_title',null,['class'=>'form-control','placeholder'=>'Meta Title','data-validation'=>'required length', 'data-validation-length' => '1-255', ])}}
						@if ($errors->has('meta_title'))
							<span class="help-block">{{ $errors->first('meta_title') }}</span>
						@endif
					</div>
				</div>
			</div>
			
			<div class="form-group {{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('meta_keywords', 'Meta Keywords',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('meta_keywords',null,['class'=>'form-control','placeholder'=>'Meta Keywords','data-validation'=>'required length', 'data-validation-length' => '1-255'])}}
						@if ($errors->has('meta_keywords'))
							<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
						@endif
					</div>
				</div>
			</div>
			
			<div class="form-group {{ $errors->has('meta_description') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('meta_description', 'Meta Description',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::textarea('meta_description',null,['class'=>'form-control','placeholder'=>'Meta Description','data-validation'=>'required length', 'data-validation-length' => '3-255'])}}
						@if ($errors->has('meta_description'))
							<span class="help-block">{{ $errors->first('meta_description') }}</span>
						@endif
					</div>
				</div>
			</div>
		
			<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('content', 'Page Content',['class'=>'col-sm-3 form-control-label requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::textarea('content',null,['class'=>'form-control ckeditor','placeholder'=>'Enter page body here....','data-validation'=>'required'])}}
						@if ($errors->has('content'))
							<span class="help-block">{{ $errors->first('content') }}</span>
						@endif
					</div>
				</div>
			</div>
			<hr/>
			<div class="form-group ">
				{{Form::submit( (isset($page->id)) ? 'Update':'Submit',['class'=>'btn btn-primary'])}}
				{{link_to(url('sitecontrol/pages/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
			</div>
		</div>
	</div>
{{Form::close()}}