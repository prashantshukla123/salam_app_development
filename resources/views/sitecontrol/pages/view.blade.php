@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">View Page Template</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/pages/lists')}}">Page Templates</a></li>
					<li class="breadcrumb-item active">View Page Template</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<dl class="row">
									<dt class="col-md-4">Title</dt>
									<dd class="col-md-8">{{$page->title}}</dd>
									<dt class="col-md-4">Meta Title</dt>
									<dd class="col-md-8">{{$page->meta_title}}</dd>
									<dt class="col-md-4">Meta Keyword</dt>
									<dd class="col-md-8">{{$page->meta_keywords}}</dd>
									<dt class="col-md-4">Status</dt>
									<dd class="col-md-8">
										@if($page->status == 1)
											<span class="badge badge-success">Active</span>
										@else
											<span class="badge badge-danger">Deactive</span>
										@endif
									</dd>
									<dt class="col-md-4">Page Content</dt>
									<dd class="col-md-8">{!! $page->content !!}</dd>
								</dl>
								{{link_to(url('sitecontrol/pages/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
							</div>
						<div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection