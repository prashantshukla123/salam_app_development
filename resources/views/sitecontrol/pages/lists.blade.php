@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Page Templates List</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item active">Page Templates List</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						{{Form::model('search',['url'=> url('sitecontrol/pages/lists'),'method'=>'post','role'=> 'form','class'=> '', 'id' => 'search_page_show_form'])}}
						{{ Form::hidden('f_filter', '1') }}
						<div class="form-group">
							<div class="row">
								<div class="col-sm-12 col-md-12 text-right">
									<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Search</a>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="collapse {{ (\session()->get('pages.f_keyword') || \session()->get('pages.f_status') != '')?'show':'' }} search" id="collapseExample">
							<div class="row">
								<div class="col-md-6 col-lg-3">
									{{Form::label('f_keyword', 'Keyword',['class'=>'form-control-label'])}}	
									{{Form::text('f_keyword',\session()->get('pages.f_keyword'),['class'=>'form-control','placeholder'=>'Search by title or subject'])}}
								</div>
								@php
									$status = '';
									if(\session()->has('pages.f_status')){
										$status = \session()->get('pages.f_status');
									}
								@endphp									
								<div class="col-md-6 col-lg-3">
									{{Form::label('f_status', 'Status',['class'=>'form-control-label'])}}
									{{Form::select('f_status',[''=>'--All--','1'=>'Active','0'=>'Inactive'],$status,['class'=>'form-control','id'=>'f_status'])}}
								</div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-lg-3" >
									<div class="form-group">
										<label class="form-control-label">&nbsp;</label><br/>
										<button type="submit" class="btn btn-success">Search</button>
										<a href="{{url('/sitecontrol/pages/lists')}}" class="btn btn-info">Reset</a>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-sm-6 col-md-3 col-lg-2 offset-sm-6 offset-md-9 offset-lg-10">
								<div class="form-group">
									{{ Form::label('rows','Per&nbsp;Page&nbsp;Records',['class' => '']) }}	{{Form::select('rows',json_decode(options['rows'],true),$limit,['class'=>'form-control','id'=>'rows', 'onchange' => '$("#search_page_show_form").submit();'])}}
								</div>
							</div>
						</div>
					</div>
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body table-responsive p-0">
						<table class="table table-hover table-bordered table-striped addedfeature">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>@sortablelink('title')</th>
									<th>@sortablelink('meta_title', 'Meta Title')</th>
									<th>@sortablelink('meta_keywords', 'Meta Keywords')</th>
									<th>@sortablelink('status')</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($pages)>=1)
									@php 
										if(isset($_GET['page'])){
											$i=($limit*$_GET['page'])-$limit;
										} else{
											$i=0;
										}
									@endphp
									@foreach($pages as $page)
										<tr class="tr_{{$page->id}}">
											<td>{{ ++$i }}</td>
											<td>{{$page->title}}</td>
											<td>{{$page->meta_title}}</td>
											<td>{{$page->meta_keywords}}</td>
											<td class="project-state">
												@if($page->status==1)
													<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Deactivate Page" title="Deactivate Page" rel="deactivate" data-id="{{$page->id}}" data-target="#StatusBox" data-url='{{ url("sitecontrol/pages/updatestatus/") }}'><span class="badge badge-success">Active</span></a>
												@else
													<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Activate Page" title="Activate Page" rel="activate" data-id="{{$page->id}}" data-target="#StatusBox" data-url='{{ url("sitecontrol/pages/updatestatus/") }}'><span class="badge badge-danger">Deactive</span></a>
												@endif
											</td>
											<td class="project-actions text-right">
												<a href="{{url('/'.$page->slug)}}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-original-title="View Detail" target="_blank" title="View Detail"><i class="fa fa-eye"></i></a>
												
												<a href="{{url('/sitecontrol/pages/edit/'.$page->id)}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Edit Detail" title="Edit Detail"><i class="fa fa-pencil-alt"></i></a>
												
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="6"><center><b>No Data Found</b></center></td>
									</tr>
								@endif
							</tbody>
						</table>
						<div class="pull-left">  {{ $pages->appends(request()->except('page'))->links() }} </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection