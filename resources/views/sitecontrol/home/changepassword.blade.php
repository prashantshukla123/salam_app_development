@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Change Password</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item active">Change Password</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if(!empty($errors->getMessages()))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5 class="errorMsg m-0">{{config('constants.formErrorMsg')}}</h5>
				</div>
				@endif
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					{{Form::model($user,['url'=>url('sitecontrol/profile/save'),'action'=>'post','role'=>'form','class'=>'form-horizontal'])}}
						@php
							echo Form::hidden('redirectPage','change-password');
							echo Form::hidden('changePassword','changePassword');
						@endphp
						<div class="card-body">
							<div class="row">
								<div class="col-md-9 col-sm-12 pr-4 d-border-right">
									<div class="form-group row{{ $errors->has('OldPassword') ? ' has-error' : '' }}">
										{{Form::label('OldPassword', 'Old Password',['class'=>'col-sm-3 col-form-label'])}}
										<div class="col-sm-9">
										{{Form::password('OldPassword',['class'=>'form-control','placeholder'=>'Old Password','data-validation' => 'required length','data-validation-length' => 'min6'])}}	
										@if ($errors->has('OldPassword'))
											<span class="help-block">{{ $errors->first('OldPassword') }}</span>
										@endif
										</div>
									</div>
									<div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
										{{Form::label('password', 'Password',['class'=>'col-sm-3 col-form-label'])}}
										<div class="col-sm-9">
										{{Form::password('password',['class'=>'form-control','placeholder'=>'Password','data-validation' => 'required length','data-validation-length' => 'min6'])}}
										@if ($errors->has('password'))
											<span class="help-block">{{ $errors->first('password') }}</span>
										@endif
										</div>
									</div>
									<div class="form-group row{{ $errors->has('confirmPassword') ? ' has-error' : '' }}">
										{{Form::label('confirmPassword', 'Confirm New Password',['class'=>'col-sm-3 col-form-label'])}}
										<div class="col-sm-9">
											{{Form::password('confirmPassword',['class'=>'form-control','placeholder'=>'Confirm New Password','data-validation' => 'required length confirmation', 'data-validation-confirm' => 'password', 'data-validation-length' => 'min6'])}}
											@if ($errors->has('confirmPassword'))
												<span class="help-block">{{ $errors->first('confirmPassword') }}</span>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer">
							{{Form::submit('Update',['class'=>'btn btn-info'])}}
							<a href="{{url('sitecontrol/dashboard')}}" class="btn btn-default float-right">Cancel</a>
						</div>
						<!-- /.card-footer -->
					{{Form::close()}}
				</div>
				<!-- /.card -->
			</div>
			<!--/.col (left) -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection