@extends('layouts.admin')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Dashboard</h1>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Artists</span>
						<span class="info-box-number">{{ $data['artistCount'] }}</span>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box mb-3">
					<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Fans</span>
						<span class="info-box-number">{{ $data['fanCount'] }}</span>
					</div>
				</div>
			</div>
			
			<div class="clearfix hidden-md-up"></div>
			
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box mb-3">
					<span class="info-box-icon bg-success elevation-1"><i class="fas fa-cubes"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Categories</span>
						<span class="info-box-number">{{ $data['categoryCount'] }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection		  