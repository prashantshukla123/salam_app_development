{{Form::model($user,['url'=>url('sitecontrol/profile/save/'.$user->id),'action'=>'post','role'=>'form','class'=>''])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		if(isset($user->id)){
			$requiredAsterisk = '';
			echo Form::hidden('id',$user->id);
			echo Form::hidden('redirectPage',$redirectPage);
		}
	@endphp
	<div class="col-sm-12">
		<div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
			{{Form::label('name', 'Name',['class'=>'col-xs-12 col-sm-6 col-md-6 col-lg-4 form-control-label requiredAsterisk'])}}
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">		{{Form::text('name',null,['class'=>'form-control','placeholder'=>'Name'])}}
				@if ($errors->has('name'))
					<span class="help-block">{{ $errors->first('name') }}</span>
				@endif
			</div>
		</div>
		<div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
			{{Form::label('email', 'Email',['class'=>'col-xs-12 col-sm-6 col-md-6 col-lg-4 form-control-label requiredAsterisk'])}}
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">		{{Form::text('email',null,['class'=>'form-control','placeholder'=>'Email'])}}
				@if ($errors->has('email'))
					<span class="help-block">{{ $errors->first('email') }}</span>
				@endif
			</div>
		</div>
		<div class="line"></div>
		<div class="form-group row">
			<label class="col-xs-12 col-sm-6 col-md-6 col-lg-4 form-control-label">&nbsp;</label>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 offset-sm-2">
				{{Form::submit('Save',['class'=>'btn btn-primary'])}}
			</div>
		</div>
	</div>
{{Form::close()}}