@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Edit Profile</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item active">Edit Profile</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if(!empty($errors->getMessages()))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5 class="errorMsg m-0">{{config('constants.formErrorMsg')}}</h5>
				</div>
				@endif
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					{{Form::model($user,['url'=>url('sitecontrol/profile/save'),'action'=>'post','role'=>'form','class'=>'form-horizontal'])}}
						@php
							echo Form::hidden('redirectPage','profile');
						@endphp
						<div class="card-body">
							<div class="row">
								<div class="col-md-9 col-sm-12 pr-4 d-border-right">
									<div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
										{{Form::label('name', 'Name',['class'=>'col-sm-2 col-form-label'])}}
										<div class="col-sm-10">
										{{Form::text('name',null,['class'=>'form-control','placeholder'=>'Name','data-validation' => 'required length','data-validation-length' => '2-255'])}}
										@if ($errors->has('name'))
											<span class="help-block">{{ $errors->first('name') }}</span>
										@endif
										</div>
									</div>
									<div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
										{{Form::label('email', 'Email',['class'=>'col-sm-2 col-form-label'])}}
										<div class="col-sm-10">
										{{Form::text('email',null,['class'=>'form-control','placeholder'=>'Email','data-validation' => 'required email'])}}
										@if ($errors->has('email'))
											<span class="help-block">{{ $errors->first('email') }}</span>
										@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="card-footer">
							{{Form::submit('Update',['class'=>'btn btn-info'])}}
							<a href="{{url('sitecontrol/dashboard')}}" class="btn btn-default float-right">Cancel</a>
						</div>
						
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection		  