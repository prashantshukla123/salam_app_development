@extends('layouts.admin')
@section('content')
<section class="content-header">
  <h1>Email Add</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				@if(!empty($errors->getMessages()))
				<div class="box-header">
					<div class="col-lg-12 bg-danger">
						<h4 class="errorMsg">{{config('constants.formErrorMsg')}}</h4>
					</div>
				</div>
				@endif
				<div class="box-body">
					@include('Admin/emails/partials/form')
				</div>
			</div>
		</div>
	</div>
</section>
</script>	  
@endsection		  