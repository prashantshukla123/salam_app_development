{{Form::model($email,['url'=>url('sitecontrol/emails/save/'.$email->id),'action'=>'post','role'=>'form','class'=>'form-horizontal form-bordered'])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		if(isset($email->id)){
			$requiredAsterisk = '';
			echo Form::hidden('id',$email->id);
		}
	@endphp
	<div class="row">
		<div class="col-md-9 col-sm-12 pr-4 d-border-right">
			<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('title', 'Title',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('title',null,['class'=>'form-control','placeholder'=>'Title','data-validation'=>'required length', 'data-validation-length' => '1-255'])}}
						@if ($errors->has('title'))
							<span class="help-block">{{ $errors->first('title') }}</span>
						@endif
					</div>
				</div>
			</div>
		
			<div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('subject', 'Email Subject',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('subject',null,['class'=>'form-control','placeholder'=>'Email Subject','data-validation'=>'required length', 'data-validation-length' => '1-255'])}}
						@if ($errors->has('subject'))
							<span class="help-block">{{ $errors->first('subject') }}</span>
						@endif
					</div>
				</div>
			</div>
		
			<div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('content', 'Email Content',['class'=>'col-sm-3 form-control-label requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::textarea('content',null,['class'=>'form-control ckeditor','placeholder'=>'Enter email body here....','data-validation'=>'required'])}}
						@if ($errors->has('content'))
							<span class="help-block">{{ $errors->first('content') }}</span>
						@endif
					</div>
				</div>
			</div>
			<hr/>
			<div class="form-group ">
				{{Form::submit( (isset($email->id)) ? 'Update':'Submit',['class'=>'btn btn-primary'])}}
				{{link_to(url('sitecontrol/emails/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
			</div>
		</div>
	</div>
{{Form::close()}}