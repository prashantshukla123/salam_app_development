{{Form::model($category,['url'=>url('sitecontrol/categories/save/'.$category->id),'action'=>'post','role'=>'form','class'=>'form-horizontal form-bordered', 'id' =>'save_category_form'])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		if(isset($category->id)){
			$requiredAsterisk = '';
			echo Form::hidden('id',$category->id, ['id'=>'rec_id']);
		}
	@endphp
	<div class="row">
		<div class="col-md-9 col-sm-12 pr-4 d-border-right">
			<div class="form-group {{ $errors->has('category_name') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('category_name', 'Name',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('category_name', null, ['class'=>'form-control','placeholder'=>'Name','data-validation'=>'required length', 'data-validation-length' => '3-100'])}}
						@if ($errors->has('category_name'))
							<span class="help-block">{{ $errors->first('category_name') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('category_slug') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('category_slug', 'Category Slug',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('category_slug', null, ['class'=>'form-control','placeholder'=>'Slug', 'readonly'])}}
						@if ($errors->has('category_slug'))
							<span class="help-block">{{ $errors->first('category_slug') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('parent_id', 'Parent Category',['class'=>'col-sm-3 col-form-label'])}}
					<div class="col-sm-9">
						{{Form::select('parent_id',$categories, null, ['class'=>'form-control','placeholder'=>'Select parent'])}}
						@if ($errors->has('parent_id'))
							<span class="help-block">{{ $errors->first('parent_id') }}</span>
						@endif
					</div>
				</div>
			</div>
			<hr/>
			<div class="form-group">
				{{Form::submit( (isset($category->id)) ? 'Update':'Submit',['class'=>'btn btn-primary'])}}
				{{link_to(url('sitecontrol/categories/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
			</div>
		</div>
	</div>
{{Form::close()}}
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/admin/common.js') }}"></script>
	
	<script type="text/javascript">
		/** Below code for make dynamic make slug accordingly title **/
		$("input[name='category_name']").blur(function(){
			var val = $(this).val();
			val = val.trim();
			val =  val.toLowerCase();
			val =  val.replace(/ /g,"-");
			val =  val.replace(/[^A-Za-z0-9^_\-]/g, "");
			
			val = makeSlug(val);
			
			$('input[name="category_slug"]').val(val);
			
		});
	</script>
@endpush