@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Categories List</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item active">Categories List</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						{{Form::model('search',['url'=> url('sitecontrol/categories/lists'),'method'=>'post','role'=> 'form','class'=> '', 'id' => 'search_category_show_form'])}}
						<div class="form-group">
							<div class="row">
								<div class="col-sm-12 col-md-12 text-right">
									<a href="{{url('sitecontrol/categories/add')}}" class="btn btn-primary btn-md ">Add Category</a>
									<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Search</a>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="collapse {{ (\session()->get('categories.f_keyword') || \session()->get('categories.f_status') != '')?'show':'' }} search" id="collapseExample">
							<div class="row">
								<div class="col-md-6 col-lg-3">
									{{Form::label('f_keyword', 'Keyword',['class'=>'form-control-label'])}}	
									{{Form::text('f_keyword',\session()->get('categories.f_keyword'),['class'=>'form-control','placeholder'=>'Search by title or subject'])}}
								</div>
								@php
									$status = '';
									if(\session()->has('categories.f_status')){
										$status = \session()->get('categories.f_status');
									}
								@endphp	
								<div class="col-md-6 col-lg-3">
									{{Form::label('f_status', 'Status',['class'=>'form-control-label'])}}
									{{Form::select('f_status',[''=>'--All--','1'=>'Active','0'=>'Inactive'],$status,['class'=>'form-control','id'=>'f_status'])}}
								</div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-lg-3" >
									<div class="form-group">
										<label class="form-control-label">&nbsp;</label><br/>
										<button type="submit" class="btn btn-success">Search</button>
										<a href="{{url('/sitecontrol/categories/lists')}}" class="btn btn-info">Reset</a>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-sm-6 col-md-3 col-lg-2 offset-sm-6 offset-md-9 offset-lg-10">
								<div class="form-group">
									{{ Form::label('rows','Per&nbsp;Page&nbsp;Records',['class' => '']) }}	{{Form::select('rows',json_decode(options['rows'],true),$limit,['class'=>'form-control','id'=>'rows', 'onchange' => '$("#search_category_show_form").submit();'])}}
								</div>
							</div>
						</div>
					</div>
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body table-responsive p-0">
						<table class="table table-hover table-bordered table-striped addedfeature">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>@sortablelink('category_name', 'Category')</th>
									<th>@sortablelink('status')</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($categories)>=1)
									@php 
										if(isset($_GET['page'])){
											$i=($limit*$_GET['page'])-$limit;
										} else{
											$i=0;
										}
									@endphp
									@foreach($categories as $category)
										<tr class="tr_{{$category->id}}">
											<td>
												{{ ++$i }}
												
												@if(!empty($category->children) && count($category->children) > 0)
													<button class="openCloseRow openBtn" data-id="{{$category->id}}">
														<i class="fa fa-plus" aria-hidden="true"></i>
													</button>
												@endif
											</td>
											<td>{{$category->category_name}}</td>
											<td class="project-state">
												@if($category->status==1)
													<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Deactivate Category" title="Deactivate Category" rel="deactivate" data-id="{{$category->id}}" data-url='{{ url("sitecontrol/categories/updatestatus/") }}' data-target="#StatusBox"><span class="badge badge-success">Active</span></a>
												@else
													<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Activate Category" title="Activate Category" rel="activate" data-id="{{$category->id}}" data-url='{{ url("sitecontrol/categories/updatestatus/") }}' data-target="#StatusBox"><span class="badge badge-danger">Deactive</span></a>
												@endif
											</td>
											<td class="project-actions text-right">
												<a href="{{url('/sitecontrol/categories/edit/'.$category->id)}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Edit Detail" title="Edit Detail"><i class="fa fa-pencil-alt"></i></a>
												
												<a href="javascript:void(0)" class="btn btn-sm btn-danger RecordDeleteClass {{(!empty($category->children) && count($category->children) > 0) ? 'hasChilds' : ''}}" data-toggle="modal" data-original-title="Delete category" title="Delete category" rel="{{ $category->id}}" data-target="#deleteBox" data-url='{{ url("sitecontrol/categories/delete/") }}'><i class="fa fa-trash"></i></a>
											</td>
										</tr>
										@if(!empty($category->children) && count($category->children) > 0)
											{!! getCatChildRow($category->children) !!}
										@endif
									@endforeach
								@else
									<tr>
										<td colspan="4"><center><b>No Data Found</b></center></td>
									</tr>
								@endif
							</tbody>
						</table>
						<div class="pull-left">  {{ $categories->appends(request()->except('page'))->links() }} </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('scripts')
	<script>
		$(document).on('click', '.openCloseRow', function(){
			var id = $(this).attr('data-id');
			var type = $(this).attr('data-type');
			
			$('.child_'+id).slideToggle();
			
			if($(this).hasClass('openBtn')){
				$(this).removeClass('openBtn').addClass('closeBtn');
				$(this).html('<i class="fa fa-minus" aria-hidden="true"></i>');
			}else{
				$(this).removeClass('closeBtn').addClass('openBtn');
				$(this).html('<i class="fa fa-plus" aria-hidden="true"></i>');
				
				if($('.child_'+id).find('.openCloseRow').hasClass('closeBtn'))
					$('.child_'+id).find('.openCloseRow').trigger('click');
			}
		});

	</script>
@endpush