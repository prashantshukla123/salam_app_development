@extends('layouts.admin_login')
@section('content')
@php
$email = ''; 
$password = ''; 
$remember_me = ''; 
if(!empty($remember)){
	pr($remember);
	$email = $remember['Username'];
	$password = $remember['Password'];
	$remember_me = 'checked'; 
}
@endphp

<div class="login-logo">
	<img src="{{asset('/uploads/logo/logo.png')}}" style="width: 100px" />
</div>


<!-- /.login-logo -->
<div class="card">
    <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form id="login_form" role="form" method="POST">
			@csrf
            <div class="input-group mb-3">
				<input class="form-control" id="username" type="text" name="Username" required="" placeholder="Enter Email" value="{{(old('email')) ? old('email') : $email}}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input class="form-control" id="password" type="password" name="Password" required="" placeholder="Enter Password" value="{{$password}}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember" class="minimal" name="remember_me" value="1" {{ $remember_me }}>
                        <label for="remember_me">
                        Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        
        <p class="mb-1">
            <a href="#">I forgot my password</a>
        </p>
    </div>
    <!-- /.login-card-body -->
</div>

@endsection		  