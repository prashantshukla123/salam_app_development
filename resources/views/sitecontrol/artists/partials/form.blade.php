@push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/admin/plugins/jquery-ui/jquery-ui.min.css') }}">
@endpush

{{Form::model($user,['url'=>url('sitecontrol/artists/save/'.$user->id),'action'=>'post','files'=>true,'role'=>'form','class'=>'form-horizontal form-bordered'])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		$image_required = 'required'; 
		if(isset($user->id)){
			$requiredAsterisk = '';
			$image_required = ''; 
			echo Form::hidden('id',$user->id);
		}
	@endphp
	<div class="row">
		<div class="col-md-9 col-sm-12 pr-4 d-border-right">
			<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('name', 'Full Name',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('name',null,['class'=>'form-control','placeholder'=>'Full Name','data-validation'=>'required length', 'data-validation-length' => 'max100'])}}
						@if ($errors->has('name'))
							<span class="help-block">{{ $errors->first('name') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('email', 'Email',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('email', null,['class'=>'form-control','placeholder'=>'Email','data-validation'=>'required email'])}}
						@if ($errors->has('email'))
							<span class="help-block">{{ $errors->first('email') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('username', 'Username',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('username', null, ['class'=>'form-control','placeholder'=>'Enter username','data-validation'=>'required alphanumeric length', 'data-validation-allowing' => ' ','data-validation-length' => "3-100"])}}
						@if($errors->has('username'))
							<span class="help-block">{{ $errors->first('username') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}" id="image_main_div">
				<div id="image_main_div" class="form-row upload-file">
					{{Form::label('image', 'Avatar',['class'=>'col-sm-3 '.$requiredAsterisk])}}
					<div class="col-sm-9">
						{{Form::file('image',['class'=>'image custom-file-input','id'=>'image','data-validation'=>$image_required.' mime size extension','data-validation-allowing'=> implode(',',config('constants.allow_img_ext')),'data-validation-max-size'=>config('constants.allow_img_size')])}}
						<label class="custom-file-label" for="image">Choose file...</label>
						@if ($errors->has('image'))
							<span class="help-block">{{ $errors->first('image') }}</span>
						@else
							<span class="help-block"></span>
						@endif
					</div>
				</div>
				<div class="form-row mt-4" style="display:{{($user->id)?'':'none'}}" id="image_show_main_div">
					<label class="col-sm-3">Current Avatar</label>
					<div class="col-sm-9" id="image_show_div">
						@if($user->artistDetails && @getimagesize(url('/uploads/artists/thumb/'.$user->artistDetails->avatar)))
							<img class="img-responsive image_list" src="{{URL::to('/uploads/artists/thumb/'.$user->artistDetails->avatar)}}" alt="Photo">
							<button type="button" class="btn btn-danger delete_image" data-id="{{ $user->id}}" data-image="{{$user->artistDetails->avatar}}" data-path="/uploads/artists/" data-id="image">Delete</button>
						@else
							<img class="img-responsive image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
						@endif
					</div>
				</div>
			</div>
		
			<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('password', 'Password',['class'=>'col-sm-3 '.$requiredAsterisk])}}
					<div class="col-sm-9">
						{{Form::password('password',['class'=>'form-control','placeholder'=>'Password','data-validation' => $image_required.' length', 'data-validation-optional' => 'true', 'data-validation-length' => 'min6'])}}
						@if ($errors->has('password'))
							<span class="help-block">{{ $errors->first('password') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
				<div class="form-row ">
					<label class="col-sm-3">&nbsp;</label>
					<div class="col-sm-9">
						<button onclick="regeneratecode()" type="button" class="btn btn-primary btn-lrg ajax" title="Generate password"><i class="fa fa-refresh"></i>&nbsp; Generate Password</button>
						<button onclick="show_password()" type="button" class="btn btn-info btn-lrg" title="view"><i class="fa fa-eye"></i>&nbsp; Show Password</button>
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('dob', 'DoB',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('artistDetails[dob]',null,['class'=>'form-control dob-date-picker','placeholder'=>'Date of birth','data-validation'=>'required date', 'data-validation-format'  => "yyyy-mm-dd", 'readonly'])}}
						@if($errors->has('dob'))
							<span class="help-block">{{ $errors->first('dob') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('status', 'Status',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::select('status',['1'=>'Active','0'=>'Inactive'],(($user) ? $user->status:null),['class'=>'form-control','id'=>'status'])}}
					</div>
				</div>
			</div>
			
			<hr/>
			<h4>Artist Details</h4>
			<br/>
			
			<div class="form-group {{ $errors->has('networkType') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('networkType', 'Where can we find you?',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::select('artistDetails[networkType]', config('constants.social_handels'),null,['class'=>'form-control','placeholder'=>'---select---','data-validation'=>''])}}
						@if($errors->has('networkType'))
							<span class="help-block">{{ $errors->first('networkType') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('social_handle') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('social_handle', 'Your Handle',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::text('artistDetails[social_handle]',null,['class'=>'form-control','placeholder'=>'your handle','data-validation'=>'length','data-validation-optional' => 'true','data-validation-length' => '3-100'])}}
						@if($errors->has('artistDetails.social_handle'))
							<span class="help-block">{{ $errors->first('artistDetails.social_handle') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('followers_count') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('followers_count', 'How many followers do you have?',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::number('artistDetails[followers_count]', null, ['class'=>'form-control','placeholder'=>'Followers counts', 'min' => 0, 'data-validation'=>'length number','data-validation-optional' => 'true','data-validation-length' => '0-100'])}}
						@if($errors->has('followers_count'))
							<span class="help-block">{{ $errors->first('followers_count') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('booking_rate') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('booking_rate', 'Booking rate',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::number('artistDetails[booking_rate]', null, ['class'=>'form-control','placeholder'=>'Enter booking rate', 'step' => '.01', 'min' => 0, 'data-validation'=>''])}}
						@if($errors->has('booking_rate'))
							<span class="help-block">{{ $errors->first('booking_rate') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('response_time') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('response_time', 'Response time',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::text('artistDetails[response_time]', null, ['class'=>'form-control','placeholder'=>'Enter response time','data-validation'=>'alphanumeric length','data-validation-optional' => 'true', 'data-validation-allowing' => ' ', 'data-validation-length' => '3-100'])}}
						@if($errors->has('response_time'))
							<span class="help-block">{{ $errors->first('response_time') }}</span>
						@endif
					</div>
				</div>
			</div>
			
			<div class="form-group {{ $errors->has('self_intro_video') ? ' has-error' : '' }}" id="image_main_div">
				<div class="form-row upload-file">
					{{Form::label('self_intro_video', 'Short intro video',['class'=>'col-sm-3 '])}}
					
					{{Form::hidden('artistDetails[self_intro_video_file_path]','')}}
					{{Form::hidden('artistDetails[self_intro_video_file_name]','')}}
					{{Form::hidden('artistDetails[self_intro_video_file_mime_type]','')}}
					
					<div id="resumable-error" style="display: none">
						Resumable not supported
					</div>
					<div class="col-sm-9" id="resumable-drop">
					
						<button type="button" class="btn btn-info" for="resumable-drop" id="choose_file" data-url = "{{ url('sitecontrol/upload') }}">Choose file...</button>
					
						@if ($errors->has('self_intro_video'))
							<span class="help-block">{{ $errors->first('self_intro_video') }}</span>
						@else
							<span class="help-block"></span>
						@endif
						<br/><br/>
						<ul id="file-upload-list" class="list-unstyled"  style="display: none"></ul>
					</div>
				</div>
				<div class="form-row mt-4" style="display:{{($user->id && !empty($user->artistDetails->self_intro_video))?'':'none'}}" id="">
					<label class="col-sm-3">Intro Video</label>
					<div class="col-sm-9" id="">
						@if($user->artistDetails && file_exists(public_path('/uploads/artists/videos/'.$user->artistDetails->self_intro_video)))
							
							<video width="350" height="250" controls>
								<source src="{{URL::to('/uploads/artists/videos/'.$user->artistDetails->self_intro_video)}}" type="video/mp4">
								Your browser does not support the video tag.
							</video>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('short_description', 'Short Description',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::textarea('artistDetails[short_description]', null, ['class'=>'form-control','placeholder'=>'Enter short description about you.','data-validation'=>'length','data-validation-optional' => 'true','data-validation-length' => '3-500'])}}
						@if($errors->has('short_description'))
							<span class="help-block">{{ $errors->first('short_description') }}</span>
						@endif
					</div>
				</div>
			</div>
			
			<div class="form-group clearfix">
				<div class="icheck-primary d-inline">
					{{Form::checkbox('artistDetails[is_available]',1,null,['id'=>'is_available'])}}
					<label for="is_available">Available</label>
				</div>
			</div>
			
			<div class="form-group">
				{{Form::submit((isset($user->id))?'Update':'Submit',['class'=>'btn btn-primary submitBtn'])}}
				{{link_to(url('sitecontrol/users/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
			</div>
		</div>
	</div>
	
	<script>

	@if(!$user->id)
		setTimeout(function(){
			$('input[name="username"]').val('');
			$('input[name="password"]').val('');
		},5);
	@endif

	$('#image').on('change',function(e){
		var fileName = e.target.files[0].name;
		$('.upload-file .custom-file-label').html(fileName);
	});

	$( function() {
		$( ".dob-date-picker" ).datepicker({
			dateFormat: "yy-mm-dd",
			minDate: new Date(1900,1-1,1), 
			maxDate: '-18Y',
			//defaultDate: new Date(1970,1-1,1),
			changeMonth: true,
			changeYear: true,
			yearRange: '-110:-18'
		});
	});
	
	$(document).ready(function(){
        var $ = window.$; // use the global jQuery instance

        var $fileUpload = $('#choose_file');
        var $fileUploadDrop = $('#resumable-drop');
        var $uploadList = $("#file-upload-list");
		
        if ($fileUpload.length > 0 && $fileUploadDrop.length > 0) {
            var resumable = new Resumable({
                // Use chunk size that is smaller than your maximum limit due a resumable issue
                // https://github.com/23/resumable.js/issues/51
                chunkSize: 2 * 1024 * 1024, // 1MB
                maxFileSize: Number('{{config("constants.allow_video_size")}}'), // 1MB
                simultaneousUploads: 1,
                maxFiles : 1,
                testChunks: false,
				fileType: ['mp4'],
                throttleProgressCallbacks: 1,
                // Get the url from data-url tag
                target: $fileUpload.data('url'),
                // Append token to the request - required for web routes
                query:{_token : $('input[name=_token]').val()}
            });

			// Resumable.js isn't supported, fall back on a different method
            if (!resumable.support) {
                $('#resumable-error').show();
            } else {
                // Show a place for dropping/selecting files
                $fileUploadDrop.show();
                resumable.assignDrop($fileUpload[0]);
                resumable.assignBrowse($fileUploadDrop[0]);

                // Handle file add event
                resumable.on('fileAdded', function (file) {
                    // Show progress pabr
                    $uploadList.html('').show();
                    // Show pause, hide resume
                    $('.resumable-progress .progress-resume-link').hide();
                    $('.resumable-progress .progress-pause-link').show();
                    // Add the file to the list
                    $uploadList.append('<li class="resumable-file-' + file.uniqueIdentifier + '">Uploading <span class="resumable-file-name"></span> <span class="resumable-file-progress"></span>');
                    $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-name').html(file.fileName);
					
                    // Actually start the upload
                    resumable.upload();
					
					$('.submitBtn').prop('disabled',true);
                });
                resumable.on('fileSuccess', function (file, message) {
					message = JSON.parse(message);
					
					//console.log(message);
					//alert('Path : '+message.path+' and name : '+message.name+' and mime_type : '+message.mime_type)
					
					$('input[name="artistDetails[self_intro_video_file_path]"]').val(message.path);
					$('input[name="artistDetails[self_intro_video_file_name]"]').val(message.name);
					$('input[name="artistDetails[self_intro_video_file_mime_type]"]').val(message.mime_type);
					
                    // Reflect that the file upload has completed
                    $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html('(completed)');
					
					$('.submitBtn').prop('disabled',false);
                });
                resumable.on('fileError', function (file, message) {
                    // Reflect that the file upload has resulted in error
                    //$('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html('(file could not be uploaded: ' + message + ')');
                    $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html('(file could not be uploaded: please try again.)');
					
					$('.submitBtn').prop('disabled',false);
                });
                resumable.on('fileProgress', function (file) {
                    // Handle progress for both the file and the overall upload
                    $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html(Math.floor(file.progress() * 100) + '%');
                    $('.progress-bar').css({width: Math.floor(resumable.progress() * 100) + '%'});
					
					$('.submitBtn').prop('disabled',true);
                });
            }

        }
		
		$(document).on('click', '.delete_image', function(){
			var id = $(this).attr('data-id');
			var image_name = $(this).attr('data-image');
			var image_path = $(this).attr('data-path');
			if(confirm('Are You sure to delete this image ?')){
				var _this = $(this);
				$.ajax({
					url:'{{url("sitecontrol/users/delete-image")}}',
					type:'post',
					headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
					dataType:'json',
					data:{'table':'artist_profiles','id':id,'name':image_name,'path':image_path},
					success:function(data){
						$.unblockUI();
						if(data.status){
							var input_id = _this.attr('data-id');
							$('#'+input_id).val(null).closest('div').find('label').html('');
							_this.closest('div').html('');
							$( "div[id*='_show_main_div']" ).slideUp();
						}
					}
				});
				
			}
		});
		
	});
	
	</script>

{{Form::close()}}

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/resumable.js/1.1.0/resumable.min.js"></script>
@endpush