@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Artists List</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item active">Artists List</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						{{Form::model('search',['url'=> url('sitecontrol/artists/lists'),'method'=>'post','role'=> 'form','class'=> '', 'id' => 'search_user_show_form'])}}
							
							<div class="form-group">
								<div class="row">
									<div class="col-sm-5 col-md-4 d-flex ">
										<h4 class="hidden-xs mr-2">
											<small>Per&nbsp;Page&nbsp;Records</small>
										</h4>{{Form::select('rows',json_decode(options['rows'],true),$limit,['class'=>'form-control','id'=>'rows', 'onchange' => '$("#search_user_show_form").submit();'])}}
									</div>
									<div class="col-sm-7 col-md-8 text-right">
										<a href="{{url('sitecontrol/artists/add')}}" class="btn btn-primary btn-md ">Add Artist</a>
										<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Search</a>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="collapse {{ (\session()->get('artists.f_keyword') || \session()->get('artists.f_status'))?'show':'' }} search" id="collapseExample">
										<div class="form-group">
											<div class="row align-items-center">
												<div class="col-lg-1 col-md-1">
													{{Form::label('f_keyword', 'Keyword',['class'=>'form-control-label'])}}	
												</div>
												<div class="col-lg-3 col-md-3">
													{{Form::text('f_keyword',\session()->get('artists.f_keyword'),['class'=>'form-control','placeholder'=>'Search by name or email'])}}
												</div>
												@php
													$status = '';
													if(\session()->has('artists.f_status')){
														$status = \session()->get('artists.f_status');
													}
												@endphp								
												<div class="col-lg-1 col-md-1">
													{{Form::label('f_status', 'Status',['class'=>'form-control-label'])}}
												</div>
												<div class="col-lg-3 col-md-3">
													{{Form::select('f_status',[''=>'--All--','1'=>'Active','0'=>'Inactive'],$status,['class'=>'form-control','id'=>'f_status'])}}
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-3" >
												<button type="submit" class="btn btn-success">Search</button>
												<a href="{{url('/sitecontrol/artists/lists')}}" class="btn btn-info">Reset</a>
											</div>	
										</div>
									</div>
								</div>
							</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					
					<div class="card-body table-responsive p-0">
						<table class="table table-hover table-bordered table-striped addedfeature">
							<thead>
								<tr>
									<th class="no-wrap">S.No.</th>
									<th class="no-wrap">Avatar</th>
									<th class="no-wrap">@sortablelink('name')</th>
									<th class="no-wrap">@sortablelink('email')</th>
									<th class="no-wrap">Phone</th>
									<th class="no-wrap">@sortablelink('status')</th>
									<th class="no-wrap">@sortablelink('created_at')</th>
									<th class="no-wrap">Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($artists)>=1)
									@php 
										if(isset($_GET['page'])){
											$i=($limit*$_GET['page'])-$limit;
										} else{
											$i=0;
										}
									@endphp
									@foreach($artists as $artist)
										
										<tr class="tr_{{$artist->id}}">
											<td>{{ ++$i }}</td>
											<td>
												@if($artist->artistDetails && @getimagesize(url('/uploads/artists/thumb/'.$artist->artistDetails->avatar)))
													<img class="img-fluid mb-2 image_list" src="{{URL::to('/uploads/artists/thumb/'.$artist->artistDetails->avatar)}}" alt="Photo">
												@else
													<img class="img-fluid mb-2 image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
												@endif
											</td>
											<td>{{$artist->name}}</td>
											<td>{{$artist->email}}</td>
											<td>{{$artist->artistDetails->phone_no}}</td>
											<td class="project-state">
												@if($artist->status==1)
													<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Deactivate artist" title="Deactivate artist" rel="deactivate" data-id="{{$artist->id}}" data-table="users" data-target="#StatusBox" data-url='{{ url("sitecontrol/artists/updatestatus/") }}'><span class="badge badge-success">Active</span></a>
												@else
													<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Activate artist" title="Activate artist" rel="activate" data-id="{{$artist->id}}" data-table="users" data-target="#StatusBox" data-url='{{ url("sitecontrol/artists/updatestatus/") }}'><span class="badge badge-danger">Deactive</span></a>
												@endif
											</td>
											<td>{{$artist->created_at}}</td>
											<td class="project-actions text-right no-wrap">
												<a href="{{url('/sitecontrol/artists/view/'.$artist->id)}}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-original-title="View Detail" title="View Detail"><i class="fa fa-eye"></i></a>
												
												<a href="{{url('/sitecontrol/artists/edit/'.$artist->id)}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Edit Detail" title="Edit Detail"><i class="fa fa-pencil-alt"></i></a>
												
												<a href="javascript:void(0)" class="btn btn-sm btn-danger RecordDeleteClass" data-toggle="modal" data-original-title="Delete artist" title="Delete artist" rel="{{ $artist->id}}" data-target="#deleteBox" data-url='{{ url("sitecontrol/artists/delete/") }}'><i class="fa fa-trash"></i></a>
												
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="8"><center><b>No Data Found</b></center></td>
									</tr>
								@endif
							</tbody>
						</table>
						<div class="pull-left">
							{{ $artists->appends(request()->except('page'))->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection