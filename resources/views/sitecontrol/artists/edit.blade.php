@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Edit Artist</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/users/lists')}}">Users</a></li>
					<li class="breadcrumb-item active">Edit Artist</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				@if(!empty($errors->getMessages()))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5 class="errorMsg m-0">{{config('constants.formErrorMsg')}}</h5>
				</div>
				@endif
				<!-- Horizontal Form -->
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<div class="card-body">
						@include('sitecontrol/artists/partials/form')
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	function show_password(){
		var e = $('input#password');
		if(e.attr('type') == 'text'){
			e.attr('type','password');
		}else{
			e.attr('type','text');
		}
	}

	function regeneratecode(){
		$('input#password').val(randomString(8));
	}
	function randomString(len, charSet) {
		charSet = charSet || '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var randomString = '';
		for (var i = 0; i < len; i++) {
			var randomPoz = Math.floor(Math.random() * charSet.length);
			randomString += charSet.substring(randomPoz,randomPoz+1);
		}
		return randomString;
	}
</script>		  
@endsection		  