@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">View Artist</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/users/lists')}}">Users</a></li>
					<li class="breadcrumb-item active">View Artist</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<dl class="row">
									<dt class="col-md-4">Name</dt>
									<dd class="col-md-8">{{$user[0]['name']}}</dd>
									<dt class="col-md-4">Email</dt>
									<dd class="col-md-8">{{$user[0]['email']}}</dd>
									<dt class="col-md-4">Username</dt>
									<dd class="col-md-8">{{$user[0]['username']}}</dd>
									<dt class="col-md-4">DoB</dt>
									<dd class="col-md-8">{{date('d M Y',strtotime($user[0]['artist_details']['dob']))}}</dd>
									<dt class="col-md-4">Status</dt>
									<dd class="col-md-8">
										@if($user[0]['status'] == 1)
											<span class="badge badge-success">Active</span>
										@else
											<span class="badge badge-danger">Deactive</span>
										@endif
									</dd>
									<dt class="col-md-4">Created</dt>
									<dd class="col-md-8">{{$user[0]['created_at']}}</dd>
									<dt class="col-md-4">Updated</dt>
									<dd class="col-md-8">{{$user[0]['updated_at']}}</dd>
								</dl>
							</div>
							<div class="col-md-12">
								<dl class="row">
									<dt class="col-md-4">Where can we find you? : </dt>
									<dd class="col-md-8">{{$user[0]['artist_details']['networkType']}}</dd>
									
									<dt class="col-md-4">Your Handel : </dt>
									<dd class="col-md-8">{{$user[0]['artist_details']['social_handle']}}</dd>
									
									<dt class="col-md-4">How many followers do you have? : </dt>
									<dd class="col-md-8">{{$user[0]['artist_details']['followers_count']}}</dd>
									
									<dt class="col-md-4">Booking rate : </dt>
									<dd class="col-md-8">{{$user[0]['artist_details']['booking_rate']}}</dd>
									
									<dt class="col-md-4">Response time : </dt>
									<dd class="col-md-8">{{$user[0]['artist_details']['response_time']}}</dd>
									
									<dt class="col-md-4">Available : </dt>
									<dd class="col-md-8">{{ ($user[0]['artist_details']['is_available'] == 1) ? 'Yes' : 'No' }}</dd>
									
									<dt class="col-md-4">Avatar : </dt>
									<dd class="col-md-8">
										@if( @getimagesize(url('/uploads/artists/thumb/'.$user[0]['artist_details']['avatar'])))
											<img class="img-responsive image_list" src="{{URL::to('/uploads/artists/thumb/'.$user[0]['artist_details']['avatar'])}}" alt="Photo">
										@else
											<img class="img-responsive image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
										@endif
									</dd>
									
									<dt class="col-md-4">Intro Video : </dt>
									<dd class="col-md-8">
										@if($user[0]['artist_details'] && file_exists(public_path('/uploads/artists/videos/'.$user[0]['artist_details']['self_intro_video'])))
							
											<video width="350" height="250" controls>
												<source src="{{URL::to('/uploads/artists/videos/'.$user[0]['artist_details']['self_intro_video'])}}" type="video/mp4">
												Your browser does not support the video tag.
											</video>
										@else
											Not uploaded.
										@endif
									</dd>
								</dl>
							</div>
							<hr>
							<div class="col-md-12">
								{{link_to(url('sitecontrol/users/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection