@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Users List</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item active">Users List</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						{{Form::model('search',['url'=> url('sitecontrol/users/lists'),'method'=>'post','role'=> 'form','class'=> '', 'id' => 'search_user_show_form'])}}
						
						<div class="row">
							<div class="col-sm-12 text-right">
								@if($user_type == 2)
									<a href="{{url('sitecontrol/users/add')}}" class="btn btn-primary btn-md ">Add Fan</a>
								@elseif($user_type == 3)
									<a href="{{url('sitecontrol/artists/add')}}" class="btn btn-primary btn-md ">Add Artist</a>
								@else
									<a href="{{url('sitecontrol/users/add')}}" class="btn btn-primary btn-md ">Add Fan</a>
									<a href="{{url('sitecontrol/artists/add')}}" class="btn btn-primary btn-md ">Add Artist</a>
								@endif
								
								<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Search</a>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="collapse {{ (\session()->get('users.f_keyword') || \session()->get('users.f_status') != '') ?'show':'' }} search" id="collapseExample">
							<div class="row">
								<div class="col-md-6 col-lg-3 ">
									<div class="form-group">
										{{Form::label('f_keyword', 'Keyword',['class'=>'form-control-label'])}}	{{Form::text('f_keyword',\session()->get('users.f_keyword'),['class'=>'form-control','placeholder'=>'Search by name or email'])}}
									</div>
								</div>
								@php
									$status = '';
									if(\session()->has('users.f_status')){
										$status = \session()->get('users.f_status');
									}
								@endphp								
								
								<div class="col-md-6 col-lg-3">
									<div class="form-group">
										{{Form::label('f_status', 'Status',['class'=>'form-control-label'])}}
										{{Form::select('f_status',[''=>'--All--','1'=>'Active','0'=>'Inactive'],$status,['class'=>'form-control','id'=>'f_status'])}}
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-lg-3" >
									<div class="form-group">
										<label class="form-control-label">&nbsp;</label><br/>
										<button type="submit" class="btn btn-success">Search</button>
										<a href="{{url('/sitecontrol/users/lists')}}" class="btn btn-info">Reset</a>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-sm-6 col-md-3 col-lg-2 offset-md-6 offset-lg-8">
								<div class="form-group">
									{{ Form::label('user_type','User Type (Artist/Fan)',['class' => '']) }}
									{{ Form::select('user_type',[2 => 'Fan', 3 => 'Artist'],$user_type,['class'=>'form-control','id'=>'user_type', 'placeholder' => 'Both', 'onchange' => '$("#search_user_show_form").submit();'])}}
								</div>
							</div>
							<div class="col-sm-6 col-md-3 col-lg-2">
								<div class="form-group">
									{{ Form::label('rows','Per&nbsp;Page&nbsp;Records',['class' => '']) }}
									{{ Form::select('rows',json_decode(options['rows'],true),$limit,['class'=>'form-control','id'=>'rows', 'onchange' => '$("#search_user_show_form").submit();'])}}
								</div>
							</div>
						</div>
					</div>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				
				<div class="card-body table-responsive p-0">
					<table class="table table-hover table-bordered table-striped addedfeature">
						<thead>
							<tr>
								<th class="no-wrap">S.No.</th>
								<th class="no-wrap">Avatar</th>
								<th class="no-wrap">@sortablelink('name')</th>
								<th class="no-wrap">@sortablelink('email')</th>
								<th class="no-wrap">Username</th>
								<th class="no-wrap">User Type</th>
								<th class="no-wrap">@sortablelink('status')</th>
								<th class="no-wrap">@sortablelink('created_at', "Created At")</th>
								<th class="no-wrap">Action</th>
							</tr>
						</thead>
						<tbody>
							@if(count($users)>=1)
								@php 
									if(isset($_GET['page'])){
										$i=($limit*$_GET['page'])-$limit;
									} else{
										$i=0;
									}
								@endphp
								@foreach($users as $user)
									<tr class="tr_{{$user->id}}">
										<td>{{ ++$i }}</td>
										<td>
										@if($user->role_id == 2)
											@if($user->fanDetails && @getimagesize(url('/uploads/users/thumb/'.$user->fanDetails->avatar)))
												<img class="img-fluid mb-2 image_list" src="{{URL::to('/uploads/users/thumb/'.$user->fanDetails->avatar)}}" alt="Photo">
											@else
												<img class="img-fluid mb-2 image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
											@endif
										@else
											@if($user->artistDetails && @getimagesize(url('/uploads/artists/thumb/'.$user->artistDetails->avatar)))
												<img class="img-fluid mb-2 image_list" src="{{URL::to('/uploads/artists/thumb/'.$user->artistDetails->avatar)}}" alt="Photo">
											@else
												<img class="img-fluid mb-2 image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
											@endif
										@endif
										</td>
										<td>{{$user->name}}</td>
										<td>{{$user->email}}</td>
										<td>{{$user->username}}</td>
										<td>
											{{($user->role_id == 2) ? 'Fan' : 'Artist'}}
										</td>
										<td class="project-state">
											@php
												$urlType = 'users';
												if($user_type == ''){
													if($user->role_id == 3)
														$urlType = 'artists';
												}elseif($user_type == 3){
													$urlType = 'artists';
												}
											@endphp
											
											@if($user->status==1)
												<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Deactivate User" title="Deactivate User" rel="deactivate" data-id="{{$user->id}}" data-table="users" data-target="#StatusBox" data-url='{{ url("sitecontrol/".$urlType."/updatestatus/") }}'><span class="badge badge-success">Active</span></a>
											@else
												<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Activate User" title="Activate User" rel="activate" data-id="{{$user->id}}" data-table="users" data-target="#StatusBox" data-url='{{ url("sitecontrol/".$urlType."/updatestatus/") }}'><span class="badge badge-danger">Deactive</span></a>
											@endif
										</td>
										<td>{{$user->created_at}}</td>
										<td class="project-actions text-right no-wrap">
											<a href="{{url('/sitecontrol/'.$urlType.'/view/'.$user->id)}}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-original-title="View Detail" title="View Detail"><i class="fa fa-eye"></i></a>
											
											<a href="{{url('/sitecontrol/'.$urlType.'/edit/'.$user->id)}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Edit Detail" title="Edit Detail"><i class="fa fa-pencil-alt"></i></a>
											
											<a href="javascript:void(0)" class="btn btn-sm btn-danger RecordDeleteClass" data-toggle="modal" data-original-title="Delete user" title="Delete user" rel="{{ $user->id}}" data-target="#deleteBox" data-url='{{ url("sitecontrol/".$urlType."/delete/") }}'><i class="fa fa-trash"></i></a>
											
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="8"><center><b>No Data Found</b></center></td>
								</tr>
							@endif
						</tbody>
					</table>
					<div class="pull-left">
						{{ $users->appends(request()->except('page'))->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection