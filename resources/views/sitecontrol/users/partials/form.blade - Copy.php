@push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/admin/plugins/jquery-ui/jquery-ui.min.css') }}">
@endpush

{{Form::model($user,['url'=>url('sitecontrol/users/save/'.$user->id),'action'=>'post','files'=>true,'role'=>'form','class'=>'form-horizontal form-bordered'])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		$image_required = 'required'; 
		if(isset($user->id)){
			$requiredAsterisk = '';
			$image_required = ''; 
			echo Form::hidden('id',$user->id);
		}
	@endphp
	<div class="row">
		<div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
			<div class="form-row">
				{{Form::label('name', 'Full Name',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
				<div class="col-sm-9">		{{Form::text('name',null,['class'=>'form-control','placeholder'=>'Full Name','data-validation'=>'required'])}}
					@if ($errors->has('name'))
						<span class="help-block">{{ $errors->first('name') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
			<div class="form-row">
				{{Form::label('email', 'Email',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
				<div class="col-sm-9">		{{Form::text('email',null,['class'=>'form-control','placeholder'=>'Email','data-validation'=>'required'])}}
					@if ($errors->has('email'))
						<span class="help-block">{{ $errors->first('email') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
			<div class="form-row">
				{{Form::label('password', 'Password',['class'=>'col-sm-3 col-form-label '.$requiredAsterisk])}}
				<div class="col-sm-9">
					{{Form::password('password',['class'=>'form-control','placeholder'=>'Password'])}}
					@if ($errors->has('password'))
						<span class="help-block">{{ $errors->first('password') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
			<div class="form-row ">
				<label class="col-sm-3 col-form-label">&nbsp;</label>
				<div class="col-sm-9">
					<button onclick="regeneratecode()" type="button" class="btn btn-primary btn-lrg ajax" title="Generate password"><i class="fa fa-refresh"></i>&nbsp; Generate Password</button>
					<button onclick="show_password()" type="button" class="btn btn-info btn-lrg" title="view"><i class="fa fa-eye"></i>&nbsp; Show Password</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6 {{ $errors->has('phone_no') ? ' has-error' : '' }}">
			<div class="form-row">
				{{Form::label('phone_no', 'Phone No',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
				<div class="col-sm-9">		{{Form::text('phone_no',(($user->fanDetails) ? $user->fanDetails->phone_no:null),['class'=>'form-control','placeholder'=>'Phone No','data-validation'=>'required'])}}
					@if($errors->has('phone_no'))
						<span class="help-block">{{ $errors->first('phone_no') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group col-md-6 {{ $errors->has('dob') ? ' has-error' : '' }}">
			<div class="form-row">
				{{Form::label('dob', 'DoB',['class'=>'col-sm-3 col-form-label requiredAsterisk'])}}
				<div class="col-sm-9">
					{{Form::text('dob',(($user->fanDetails) ? $user->fanDetails->dob:null),['class'=>'form-control dob-date-picker','placeholder'=>'Date of birth','data-validation'=>'required'])}}
					@if($errors->has('dob'))
						<span class="help-block">{{ $errors->first('dob') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="row {{($user->id)?'':'d-none'}}" id="image_show_main_div">
		<div class="form-group col-md-6">
			<div class="form-row">
				<label class="col-sm-3 col-form-label">Current Avatar</label>
				<div class="col-sm-9" id="image_show_div">
					@if($user->fanDetails && @getimagesize(url('/uploads/users/thumb/'.$user->fanDetails->avatar)))
						<img class="img-responsive image_list" src="{{URL::to('/uploads/users/thumb/'.$user->fanDetails->avatar)}}" alt="Photo">
					@else
						<img class="image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6 {{ $errors->has('image') ? ' has-error' : '' }}" id="image_main_div">
			<div class="form-row upload-file">
				{{Form::label('image', 'Avatar',['class'=>'col-sm-3 col-form-label '.$requiredAsterisk])}}
				<div class="col-sm-9">
					{{Form::file('image',['class'=>'image custom-file-input','id'=>'image','data-validation'=>$image_required.' mime size extension','data-validation-allowing'=>'jpg,png,gif','data-validation-max-size'=>'512kb'])}}
					<label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
					@if ($errors->has('image'))
						<span class="help-block">{{ $errors->first('image') }}</span>
					@else
						<span class="help-block"></span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group col-md-6 {{ $errors->has('status') ? ' has-error' : '' }}">
			<div class="form-row">
				{{Form::label('status', 'Status',['class'=>'col-sm-3 col-form-label'])}}
				<div class="col-sm-9">
					{{Form::select('status',['1'=>'Active','0'=>'Inactive'],(($user) ? $user->status:null),['class'=>'form-control','id'=>'status'])}}
				</div>
			</div>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="form-group col-md-6">
			<label class="col-sm-3 col-form-label">&nbsp;</label>
			<div class="col-sm-9 offset-sm-2">
				{{Form::submit((isset($user->id))?'Update':'Submit',['class'=>'btn btn-primary'])}}
				{{link_to(url('sitecontrol/users/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
			</div>
		</div>
	</div>
<script>
@if(!$user->id)
	setTimeout(function(){
		$('input[name="username"]').val('');
		$('input[name="password"]').val('');
	},5);
@endif
$('#image').on('change',function(e){
	var fileName = e.target.files[0].name;
	$('.upload-file .custom-file-label').html(fileName);
});
$( function() {
	$( ".dob-date-picker" ).datepicker({
		dateFormat: "yy-mm-dd",
		minDate: new Date(1900,1-1,1), 
		maxDate: '-18Y',
		//defaultDate: new Date(1970,1-1,1),
		changeMonth: true,
		changeYear: true,
		yearRange: '-110:-18'
	});
});
</script>
{{Form::close()}}

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
@endpush