@push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/admin/plugins/jquery-ui/jquery-ui.min.css') }}">
@endpush

{{Form::model($user,['url'=>url('sitecontrol/users/save/'.$user->id),'action'=>'post','files'=>true,'role'=>'form','class'=>'form-horizontal form-bordered'])}}
	@php
		$requiredAsterisk = 'requiredAsterisk';
		$image_required = 'required'; 
		if(isset($user->id)){
			$requiredAsterisk = '';
			$image_required = ''; 
			echo Form::hidden('id',$user->id);
		}
	@endphp
	<div class="row">
		<div class="col-md-9 col-sm-12 pr-4 d-border-right">
			<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('name', 'Full Name',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">		{{Form::text('name',null,['class'=>'form-control','placeholder'=>'Full Name','data-validation'=>'required length', 'data-validation-length'=>'max100'])}}
						@if ($errors->has('name'))
							<span class="help-block">{{ $errors->first('name') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('email', 'Email',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('email', null,['class'=>'form-control','placeholder'=>'Email','data-validation'=>'required email'])}}
						@if ($errors->has('email'))
							<span class="help-block">{{ $errors->first('email') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('username', 'Username',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('username', null, ['class'=>'form-control','placeholder'=>'Enter Username','data-validation'=>'required alphanumeric length','data-validation-length' => "3-100"])}}
						@if($errors->has('username'))
							<span class="help-block">{{ $errors->first('username') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}" id="image_main_div">
				<div id="image_main_div" class="form-row upload-file">
					{{Form::label('image', 'Avatar',['class'=>'col-sm-3 '.$requiredAsterisk])}}
					<div class="col-sm-9">
						{{Form::file('image',['class'=>'image custom-file-input','id'=>'image','data-validation'=>$image_required.' mime size extension','data-validation-allowing'=> implode(',',config('constants.allow_img_ext')),'data-validation-max-size'=>config('constants.allow_img_size')])}}
						<label class="custom-file-label" for="image">Choose file...</label>
						@if ($errors->has('image'))
							<span class="help-block">{{ $errors->first('image') }}</span>
						@else
							<span class="help-block"></span>
						@endif
					</div>
				</div>
				<div class="form-row mt-4" style="display:{{($user->id)?'':'none'}}" id="image_show_main_div">
					<label class="col-sm-3">Current Avatar</label>
					<div class="col-sm-9" id="image_show_div">
						@if($user->fanDetails && @getimagesize(url('/uploads/users/thumb/'.$user->fanDetails->avatar)))
							<img class="img-responsive image_list delete_image" src="{{URL::to('/uploads/users/thumb/'.$user->fanDetails->avatar)}}" alt="Photo" />
							<button type="button" class="btn btn-danger delete_image" data-id="{{ $user->id}}" data-image="{{$user->fanDetails->avatar}}" data-path="/uploads/users/" data-id="image">Delete</button>
						@else
							<img class="img-responsive image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
						@endif
					</div>
				</div>
			</div>
		
			<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('password', 'Password',['class'=>'col-sm-3 '.$requiredAsterisk])}}
					<div class="col-sm-9">
						{{Form::password('password',['class'=>'form-control','placeholder'=>'Password','data-validation' => $image_required.' length', 'data-validation-optional' => 'true', 'data-validation-length' => 'min6'])}}
						@if ($errors->has('password'))
							<span class="help-block">{{ $errors->first('password') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
				<div class="form-row ">
					<label class="col-sm-3">&nbsp;</label>
					<div class="col-sm-9">
						<button onclick="regeneratecode()" type="button" class="btn btn-primary btn-lrg ajax" title="Generate password"><i class="fa fa-refresh"></i>&nbsp; Generate Password</button>
						<button onclick="show_password()" type="button" class="btn btn-info btn-lrg" title="view"><i class="fa fa-eye"></i>&nbsp; Show Password</button>
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('dob', 'DoB',['class'=>'col-sm-3 requiredAsterisk'])}}
					<div class="col-sm-9">
						{{Form::text('fanDetails[dob]',null,['class'=>'form-control dob-date-picker','placeholder'=>'Date of birth','data-validation'=>'required date', 'data-validation-format'  => "yyyy-mm-dd", 'readonly'])}}
						@if($errors->has('dob'))
							<span class="help-block">{{ $errors->first('dob') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
				<div class="form-row">
					{{Form::label('status', 'Status',['class'=>'col-sm-3'])}}
					<div class="col-sm-9">
						{{Form::select('status',['1'=>'Active','0'=>'Inactive'],(($user) ? $user->status:null),['class'=>'form-control','id'=>'status'])}}
					</div>
				</div>
			</div>
			<hr/>
			<div class="form-group col-md-6">
				{{Form::submit((isset($user->id))?'Update':'Submit',['class'=>'btn btn-primary submitBtn'])}}
				{{link_to(url('sitecontrol/users/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
			</div>
		</div>
	</div>
	
	<script>

	@if(!$user->id)
		setTimeout(function(){
			$('input[name="username"]').val('');
			$('input[name="password"]').val('');
		},5);
	@endif

	$('#image').on('change',function(e){
		var fileName = e.target.files[0].name;
		$('.upload-file .custom-file-label').html(fileName);
	});

	$( function() {
		$( ".dob-date-picker" ).datepicker({
			dateFormat: "yy-mm-dd",
			minDate: new Date(1900,1-1,1), 
			maxDate: '-18Y',
			//defaultDate: new Date(1970,1-1,1),
			changeMonth: true,
			changeYear: true,
			yearRange: '-110:-18'
		});
		
		$(document).on('click', '.delete_image', function(){
			var id = $(this).attr('data-id');
			var image_name = $(this).attr('data-image');
			var image_path = $(this).attr('data-path');
			if(confirm('Are You sure to delete this image ?')){
				var _this = $(this);
				$.ajax({
					url:'{{url("sitecontrol/users/delete-image")}}',
					type:'post',
					headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
					dataType:'json',
					data:{'table':'fan_profiles','id':id,'name':image_name,'path':image_path},
					success:function(data){
						$.unblockUI();
						if(data.status){
							var input_id = _this.attr('data-id');
							$('#'+input_id).val(null).closest('div').find('label').html('');
							_this.closest('div').html('');
							$( "div[id*='_show_main_div']" ).slideUp();
						}
					}
				});
				
			}
		});
	});
	</script>

{{Form::close()}}

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/resumable.js/1.1.0/resumable.min.js"></script>
@endpush