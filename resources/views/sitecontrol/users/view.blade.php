@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">View User</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/users/lists')}}">Users</a></li>
					<li class="breadcrumb-item active">View User</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">
								<dl class="row">
									<dt class="col-md-4">Name</dt>
									<dd class="col-md-8">{{$user[0]['name']}}</dd>
									<dt class="col-md-4">Email</dt>
									<dd class="col-md-8">{{$user[0]['email']}}</dd>
									<dt class="col-md-4">Username</dt>
									<dd class="col-md-8">{{$user[0]['username']}}</dd>
									<dt class="col-md-4">DoB</dt>
									<dd class="col-md-8">{{date('d M Y',strtotime($user[0]['fan_details']['dob']))}}</dd>
									<dt class="col-md-4">Status</dt>
									<dd class="col-md-8">
										@if($user[0]['status'] == 1)
											<span class="badge badge-success">Active</span>
										@else
											<span class="badge badge-danger">Deactive</span>
										@endif
									</dd>
									<dt class="col-md-4">Created</dt>
									<dd class="col-md-8">{{$user[0]['created_at']}}</dd>
									<dt class="col-md-4">Updated</dt>
									<dd class="col-md-8">{{$user[0]['updated_at']}}</dd>
								</dl>
								{{link_to(url('sitecontrol/users/lists'), 'Back to list', ['class'=>'btn btn-warning'])}}
							</div>
							<div class="col-md-6">
								@if(@getimagesize(url('/uploads/users/thumb/'.$user[0]['fan_details']['avatar'])))
									<img class="img-responsive image_list" src="{{URL::to('/uploads/users/thumb/'.$user[0]['fan_details']['avatar'])}}" alt="Photo">
								@else
									<img class="img-responsive image_list" src="{{URL::to('/img/admin/no_image.png')}}" alt="Photo">
								@endif
							</div>
						<div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection