@extends('layouts.admin_login')
@section('content')

	@php
		$email = ''; 
		$password = ''; 
		$remember_me = ''; 
		if(!empty($remember)){
			//pr($remember);
			$email = $remember['Username'];
			$password = $remember['Password'];
			$remember_me = 'checked'; 
		}
	@endphp

	<div class="login-logo">
		@if(!empty($options_array['logo']) && @getimagesize(public_path('/uploads/logo/'.$options_array['logo'])) && @file_exists(public_path('/uploads/logo/'.$options_array['logo'])))
			<img src="{{asset('/uploads/logo/'.$options_array['logo'])}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8;width:100px" />
		@else
			<span class="brand-text font-weight-light">
				{{ (!empty($options_array['site_name'])) ? $options_array['site_name'] : env('APP_NAME') }}
			</span>
		@endif
		
	</div>
	
	<div class="card">
		<div class="card-body login-card-body">
			<p class="login-box-msg">Sign in to start your session</p>
			<form id="login_form" role="form" method="POST">
				@csrf
				<div class="form-group mb-3">
					<div class="input-group">
						<input class="form-control" id="username" type="text" name="Username" required="" placeholder="Enter Email" value="{{(old('email')) ? old('email') : $email}}" data-validation="required email length" data-validation-length="max50" />
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-envelope"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group mb-3">
					<div class="input-group">
						<input class="form-control" id="password" type="password" name="Password" required="" placeholder="Enter Password" value="{{$password}}" data-validation="required length" data-validation-length="min6" />
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-8">
						<div class="icheck-primary">
							<input type="checkbox" id="remember_me" class="minimal" name="remember_me" value="1" {{ $remember_me }}>
							<label for="remember_me">
							Remember Me
							</label>
						</div>
					</div>
					<div class="col-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div>
				</div>
			</form>
			
			@if (Route::has('password.request'))
				<a class="btn-link" href="{{ route('password.request') }}">
					{{ __('Forgot Your Password?') }}
				</a>
			@endif
		</div>
	</div>

@endsection		  