@extends('layouts.admin')

@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Admin Setting</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('sitecontrol/dashboard')}}">Home</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if(!empty($errors->getMessages()))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5 class="errorMsg m-0">{{config('constants.formErrorMsg')}}</h5>
				</div>
				@endif
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">&nbsp;</h3>
					</div>
					
					<div class="card-body">
						@php
							$image_required = '';
							$requiredAsterisk = 'requiredAsterisk';
							if(isset($options['logo']) && !empty($options['logo'])){
								$image_required = '';
								$requiredAsterisk = '';
							}
						@endphp
						{{Form::model('setting',['url'=> url('sitecontrol/setting/admin/'),'action'=>'post','files'=>true,'role'=>'form','class'=>''])}}
						<div class="col-xs-12">
							<div class="form-group row{{ $errors->has('site_name') ? ' has-error' : '' }}">
								{{Form::label('site_name', 'Site Name',['class'=>'col-sm-2 form-control-label requiredAsterisk'])}}
								<div class="col-sm-8 col-md-6 col-lg-4">
									{{Form::text('site_name',(array_key_exists('site_name',old()))? old('site_name'):$options['site_name'],['class'=> 'form-control','placeholder'=> 'Site Name','data-validation'=> 'required'])}}
									@if ($errors->has('site_name'))
										<span class="help-block">{{ $errors->first('site_name') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group row{{ $errors->has('admin_mail') ? ' has-error' : '' }}">
								{{Form::label('admin_mail', 'Admin Mail',['class'=>'col-sm-2 form-control-label requiredAsterisk'])}}
								<div class="col-sm-8 col-md-6 col-lg-4">
									{{Form::text('admin_mail',(array_key_exists('admin_mail',old()))? old('admin_mail'):$options['admin_mail'],['class'=> 'form-control','placeholder'=> 'Admin Mail','data-validation'=> 'required'])}}
									@if ($errors->has('admin_mail'))
										<span class="help-block">{{ $errors->first('admin_mail') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group row{{ $errors->has('admin_department') ? ' has-error' : '' }}">
								{{Form::label('admin_department', 'Admin Department Name',['class'=>'col-sm-2 form-control-label requiredAsterisk'])}}
								<div class="col-sm-8 col-md-6 col-lg-4">
									{{Form::text('admin_department',(array_key_exists('admin_department',old()) )? old('admin_department'):$options['admin_department'],['class'=> 'form-control','placeholder'=> 'AdminDepartment Name','data-validation'=> 'required'])}}
									@if ($errors->has('admin_department'))
										<span class="help-block">{{ $errors->first('admin_department') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group row{{ $errors->has('ADMIN_PAGE_LIMIT') ? ' has-error' : '' }}">
								{{Form::label('ADMIN_PAGE_LIMIT', 'Admin Page Limit',['class'=>'col-sm-2 form-control-label requiredAsterisk'])}}
								<div class="col-sm-8 col-md-6 col-lg-4">									{{Form::text('ADMIN_PAGE_LIMIT',(array_key_exists('ADMIN_PAGE_LIMIT',old()))?  old('ADMIN_PAGE_LIMIT'):$options['ADMIN_PAGE_LIMIT'],['class'=> 'form-control','placeholder'=> 'Site Name','data-validation'=> 'required'])}}
									@if ($errors->has('ADMIN_PAGE_LIMIT'))
										<span class="help-block">{{ $errors->first('ADMIN_PAGE_LIMIT') }}</span>
									@endif
								</div>
							</div>
							<div id="logo_main_div" class="form-group row{{ $errors->has('logo') ? ' has-error' : '' }}">
								{{Form::label('logo', 'Site Logo',['class'=>'col-sm-2 form-control-label '.$requiredAsterisk])}}
								<div class="col-sm-8 col-md-6 col-lg-4">
									{{Form::file('logo',['class'=>'image','id'=>'logo','data-validation'=>$image_required.' mime size extension','data-validation-allowing'=>'jpeg,jpg,png,gif','data-validation-max-size'=>'1mb'])}}
									@if ($errors->has('logo'))
										<span class="help-block">{{ $errors->first('logo') }}</span>
									@else
										<span class="help-block"></span>
									@endif
								</div>
							</div>
							<div class="form-group row" id="logo_show_main_div">
								<label class="col-sm-2 form-control-label">&nbsp;</label>
								<div class="col-sm-8 col-md-6 col-lg-4 image_main_box" id="logo_show_div">
									@if(@getimagesize(url('/uploads/logo/'.$options['logo'])))
										<label>
											<img class="img-fluid mb-2 image_list" src="{{URL::to('/uploads/logo/'.$options['logo'])}}" alt="Photo">
										</label>
									@endif
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-8 offset-sm-2">
									{{Form::submit('Save',['class'=>'btn btn-primary'])}}
								</div>
							</div>
						</div>
						{{Form::close()}}
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

<script src="{{ asset('js/admin/common.js') }}"></script>  
@endsection		  