@extends('layouts.admin_login')

@section('content')
	<div class="login-logo">
		@if(!empty($options_array['logo']) && @getimagesize(public_path('/uploads/logo/'.$options_array['logo'])) && @file_exists(public_path('/uploads/logo/'.$options_array['logo'])))
			<img src="{{asset('/uploads/logo/'.$options_array['logo'])}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8;width:100px" />
		@else
			<span class="brand-text font-weight-light">
				{{ (!empty($options_array['site_name'])) ? $options_array['site_name'] : env('APP_NAME') }}
			</span>
		@endif
		
	</div>
	
	<div class="card">
		<div class="card-body login-card-body">
			<p class="login-box-msg">{{ __('Reset Password') }}</p>
			@if (session('status'))
				<div class="alert alert-success" role="alert">
					{{ session('status') }}
				</div>
			@endif
			<form method="POST" action="{{ route('password.email') }}">
				@csrf
				<div class="form-group mb-3">
					<label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
					<div class="input-group">
						<input class="form-control @error('email') is-invalid @enderror" id="email" type="text" name="email" required autocomplete="email" autofocus placeholder="Enter Email" value="{{old('email')}}" data-validation="required email length" data-validation-length="max50" />
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-envelope"></span>
							</div>
						</div>
					</div>
					@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				
				<div class="form-group mb-3">
					<a class="btn-link mr-4" href="{{ route('siteControlLogin') }}">Back to login</a>
					<button type="submit" class="btn btn-primary">
						{{ __('Send Password Reset Link') }}
					</button>
				</div>
			</form>
		</div>
	</div>
@endsection
