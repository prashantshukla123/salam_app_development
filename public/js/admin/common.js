if($('.ckeditor').length > 0){
	CKEDITOR.config.extraAllowedContent = 'li span;ul;li;table;td;style;*[id];*(*);*{*};* *';
	CKEDITOR.config.allowedContent = true;
	CKEDITOR.replaceClass = 'ckeditor';
}

$(document).ready(function(){
	$.validate({ 
		modules : 'file, sanitize, security',
		inlineErrorMessageCallback: function($input, errorMessage, config) {
			if (errorMessage) {
				if($input.closest('div').hasClass('input-group')){
					$input.closest('div').nextAll().remove();
					
					$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input.closest('div'));
				}else if($input.closest('div').hasClass('custom-checkbox')){
					$input.closest('div').nextAll().remove();
					
					$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input.closest('div'));
				}else if($input.hasClass('custom-file-input')){
					$input.closest('div').find('span').remove();
					
					$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input.closest('div').find('label'));
				}else{
					$input.nextAll().remove();
					$('<span class="help-block text-danger">'+errorMessage+'</span>').insertAfter($input);
				}
			}else {
				if($input.hasClass('custom-file-input')){
					$input.closest('div').find('span').remove();
				}else{
					$input.nextAll().remove();
				}
			}
			return false; // prevent default behaviour
		},
		submitErrorMessageCallback : function($form, errorMessages, config) {
			/* if (errorMessages) {
				customDisplayErrorMessagesInTopOfForm($form, errorMessages);
			} else {
				customRemoveErrorMessagesInTopOfForm($form);
			}
			return false; // prevent default behaviour */
		}
	});
	
	$('input, textarea').on('beforeValidation', function(value, lang, config) {
		if(this.type !== 'file'){
			this.value = this.value.trim();
			if (this.value.length > 0) {
				var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
				while (SCRIPT_REGEX.test(this.value)) {
					val = this.value.replace(SCRIPT_REGEX, "");
					this.value = $.trim( val );
				}
			}
			this.value = this.value;
		}
    });
	
	$(function() {
		// Multiple images preview in browser
		var imagesPreview = function(input,main_div, remove_div, placeToInsertImagePreview,input_id,panel) {
			
			if (input.files) {
				var filesAmount = input.files.length;
				for (i = 0; i < filesAmount; i++) {
					var reader = new FileReader();
					var tarr = input.files[i].name.split('/');
					var file = tarr[tarr.length-1];
					var data = file.split('.');
					var data = data[data.length-1];
					//alert(data);
					var noError= true;
					if ($.inArray(data, ['jpg', 'jpeg','png','gif']) == -1) {
						noError = false;
						return false;
					}
					
					if(noError){
						reader.onload = function(event) {
							$('#'+main_div+' span').html('');
							
							if(panel){
								var clone = '<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2"><div class="image_main_box"><label for=""><div class="square"><img class="img-responsive" src="'+event.target.result+'" alt="Photo"></div></label></div></div>';
							}else{
								var clone = '<img class="img-responsive image_list" src="'+event.target.result+'" alt="Photo" style="width:150px"> <button type="button" class="btn btn-danger new_upload" data-id="'+input_id+'">Delete</button>';
							}
							//alert(clone);
							$(placeToInsertImagePreview).append(clone);
							$('div#'+remove_div).removeClass('hide');
							
							$('div#'+remove_div).slideDown();
						}
						reader.readAsDataURL(input.files[i]);
					}
				}
			}
		};
		
		$(document).on('change','.image', function(){
			var this_id = $(this).attr('id');
			$('div#'+this_id+'_show_div').html('');
			
			if($(this).hasClass('image_panel')){
				var panel = true;
			}else{
				var panel = false;
			}
			
			imagesPreview(this,this_id+'_main_div',this_id+'_show_main_div', 'div#'+this_id+'_show_div',this_id,panel);
		});
		
		$(document).on('click','.new_upload', function(){
			var input_id = $(this).attr('data-id');
			$('#'+input_id).val(null).closest('div').find('label').html('');
			$(this).closest('div').html('');
			$( "div[id*='_show_main_div']" ).slideUp();
		});
	});
	
	$(document).on('blur', 'input[name="password"]', function(){
		$(this).attr('type','password')
	});
});

$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI).ready(function(){
	var col = $('.card-body table').find('thead tr th').length;
	var row = $('.card-body table').find('tbody tr td').length;
	
	if(row < 2){
		$('.card-body table').find('tbody tr td').attr('colspan',col);
	}
	
	$(document).on('click','.change_status_old',function(index,value){
		var id = $(this).attr('data-id');
		var table = $(this).attr('data-table');
		var status = $(this).attr('data');
		if(status == 1){
			status = 0;
			var btn_class = 'label-primary';
			var label = 'Inactive';
		}else{
			status = 1;
			var btn_class = 'label-success';
			var label = 'Active';
		}
		$.ajax({
			url:SITE_URL+'/admin/actions/update',
			type:'post',
			headers: {'X-CSRF-TOKEN': csrf_token},
			dataType:'json',
			data:{'table':table,'whereField':'id','whereValue':id,'updateFields':{0:{'updateField':'status','updateValue':status}}},
			success:function(data){
				if(data.status == 'success'){
					var str = '<div class="btn label btn-sm '+btn_class+' change_status status_btn_'+id+'" data="'+status+'" data-id="'+id+'" data-table="'+table+'" data-toggle="tooltip" data-original-title="Status">'+label+'</div>';
					$('.status_btn_'+id).replaceWith(str);
					
					$('[data-toggle="tooltip"]').tooltip(); 
				}
			}
		});
	});
	
	$(document).on('click','.delete_old',function(index,value){
		var id = $(this).attr('data-id');
		var dataClosestClass = '';
		
		if($(this).hasAttr('data-place')) {
			var dataPlace = $(this).attr('data-place');
			var dataRemoveBox = '';
			var _this = $(this);
		}else if($(this).hasAttr('data-remove-box')){
			var dataRemoveBox = $(this).attr('data-remove-box');
			var dataPlace = '';
			dataClosestClass = $(this).attr('data-closest-class');
			var _this = $(this);
			
			if($('div.'+dataClosestClass).length < 2){
				alert('At least one option is required.');
				return false;
			}
			
			if(!_this.hasClass('old_section')){
				_this.closest('.'+dataClosestClass).remove();
				return false;
			}
		} else {
			var dataPlace = '';
			var dataRemoveBox = '';
			var _this = $(this).closest('table');
		}
		
		var str = $(this).attr('data');
		str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
			return letter.toUpperCase();
		});
		
		var data = null;
		
		if($(this).attr('data-relations')){
			data = $.parseJSON($(this).attr('data-relations'));
		}
		
		//console.log(data);
		
		if(confirm("You are about to delete '"+str+".' Are you sure?")){
			$.ajax({
				url:SITE_URL+'/admin/actions/delete',
				type:'post',
				headers: {'X-CSRF-TOKEN': csrf_token},
				dataType:'json',
				data:data,
				success:function(data){
					if(data.status == 'success'){
						if(dataPlace != ''){
							_this.closest('.videoBoxDiv').remove();
							if($('.videoBoxDiv').length < 1){
								var msg = '<center><h3>No more videos found here...</h3></center>';
								$('.alldemo').append(msg);
							}
						}else if(dataRemoveBox != ''){
							_this.closest('.'+dataClosestClass).remove();
						}else{
							$('.tr_'+id).remove();
							
							var col = _this.find('thead tr th').length;
							if(_this.find('tbody tr').length < 1){
								var msg = '<tr><td colspan="'+col+'"><center><b>No more data found here...</b></center></td></tr>';
								_this.find('tbody').append(msg);
							}
						}
					}
				}
			});
		}
	});
	
	// Delete click Update	
	$(document).on('click', '.RecordDeleteClass', function(){
		id = $(this).attr('rel');
		url = $(this).data('url');
		$('#recordDeleteID').val(id);
		$('#RecordDeleteForm').attr('action', url);
		
		if($(this).hasClass('hasChilds')){
			var str = '<p>If you delete this then its related sub categories also will be deleted.</p>';
			$('#RecordDeleteForm').find('div.modal-body').append(str);
		}
	});
	
	$(document).on('click', '.RecordUpdateClass', function(){
		id = $(this).data('id');
		url = $(this).data('url');
		rel = $(this).attr('rel');
		
		$('#recordID').val(id);
		
		if(rel == 'activate'){
			$('#recordStatus').val(1);
		}else{
			$('#recordStatus').val(0);
		}
		
		$('#RecordStatusFormId').attr('action', url);
		
		$('#statusname').text(rel);
	});
	
	$.fn.hasAttr = function(name) {  
	   return this.attr(name) !== undefined;
	};
});

function makeSlug(string) {
  const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
  const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return string.toString().toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}