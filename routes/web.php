<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//------------------ sitecontrol Routes --------------------------
	
Route::group(['prefix' => 'sitecontrol','namespace'=>'sitecontrol'], function () { 
	Route::get('/', function() {return redirect('/sitecontrol/login');} );
	Route::any('login', 'UserController@login')->name('siteControlLogin');
	Route::any('forgot-password', 'UserController@forgotpassword');
	Route::any('logout', function(){ Auth::logout();return redirect('/sitecontrol/login');} );
});	
	
Route::group( [ 'prefix' => 'sitecontrol','as' => 'sitecontrol' ,'middleware' => ['admin','setOptions'],'namespace'=>'sitecontrol'], function(){
	Route::post('upload', 'UploaderChunkController@upload' );
	
	Route::any('dashboard', 'HomeController@index' );
	Route::get('profile', 'HomeController@myaccount' );
	Route::post('profile/save/{id?}', 'HomeController@saveProfile' );
	Route::get('change-password', 'HomeController@changepassword' );
	
	/** User Manager URL's **/
	Route::group(['prefix'=>'users','as'=>'users'], function(){
		Route::any('lists', 'UserController@lists')->name('userList');
		Route::get('add', 'UserController@addForm' );
		Route::any('edit/{id}', 'UserController@edit' );
		Route::post('delete', 'UserController@delete' );
		Route::post('updatestatus', 'UserController@updatestatus' );
		Route::any('view/{id}', 'UserController@view' );
		
		Route::get('export', 'UserController@export')->name('userExport');
		
		/** Form post URL **/
		Route::post('save/{id?}', 'UserController@saveForm' );
		
		/** Ajax image delete URL **/
		Route::post('delete-image/', 'UserController@deleteImage' );
	});
	
	/** Artists Manager URL's **/
	Route::group(['prefix'=>'artists','as'=>'artists'], function(){
		Route::any('lists', 'ArtistsController@lists')->name('artistList');
		Route::get('add', 'ArtistsController@addForm' )->name('artistAdd');
		Route::get('edit/{id}', 'ArtistsController@edit')->name('artistEdit');
		Route::post('delete', 'ArtistsController@delete')->name('artistDelete');
		Route::post('updatestatus', 'ArtistsController@updatestatus')->name('artistUpdateStatus');
		Route::any('view/{id}', 'ArtistsController@view')->name('artistView');
		
		/** Form post URL **/
		Route::post('save/{id?}', 'ArtistsController@saveForm')->name('artistSave');
	});
	
	/** Category Manager URL's **/
	Route::group(['prefix'=>'categories','as'=>'categories'], function(){
		Route::any('lists', 'CategoriesController@lists')->name('categoryList');
		Route::get('add', 'CategoriesController@addForm' );
		Route::any('edit/{id}', 'CategoriesController@edit' );
		Route::post('updatestatus', 'CategoriesController@updatestatus' );
		Route::any('view/{id}', 'CategoriesController@view' );
		Route::post('delete', 'CategoriesController@delete')->name('categoryDelete');
		
		/** Form post URL **/
		Route::post('save/{id?}', 'CategoriesController@saveForm' );
		Route::post('checkunique', 'CategoriesController@checkunique' );
	});
	
	/** EmailTemplate Manager URL's **/
	Route::group(['prefix'=>'emails','as'=>'emails'], function(){
		Route::any('lists', 'EmailsController@lists')->name('emailList');
		Route::any('edit/{id}', 'EmailsController@edit' );
		Route::post('updatestatus', 'EmailsController@updatestatus' );
		Route::any('view/{id}', 'EmailsController@view' );
		
		/** Form post URL **/
		Route::post('save/{id?}', 'EmailsController@saveForm' );
	});
	
	/** Static Page Manager URL's **/
	Route::group(['prefix'=>'pages','as'=>'pages'], function(){
		Route::any('lists', 'PagesController@lists')->name('pageList');
		Route::any('edit/{id}', 'PagesController@edit' );
		Route::post('updatestatus', 'PagesController@updatestatus' );
		Route::any('view/{id}', 'PagesController@view' );
		
		/** Form post URL **/
		Route::post('save/{id?}', 'PagesController@saveForm' );
	});
	
	/** Sit setting Manager URL's **/
	Route::group(['prefix'=>'setting','as'=>'setting'], function(){
		Route::get('admin', 'SettingsController@admin' );
		Route::post('admin', 'SettingsController@saveAdminForm' );
	});
});

//------------------ frontend Routes --------------------------

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/email', 'HomeController@email');
Route::get('/upload-form', 'UploadController@form');
Route::post('/upload', 'UploadController@upload');

Route::get('{slug}', [
    'uses' => 'HomeController@view' 
])->where('slug', '([A-Za-z0-9\-\/]+)');;

/* Function for print array in formated form */
if(!function_exists('pr')){
	function pr($array){
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
}
	
/* Function for print query log */
if(!function_exists('qLog')){
	DB::enableQueryLog();
	function qLog(){
		pr(DB::getQueryLog());
	}
}