-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2019 at 06:34 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latest_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `fan_profiles`
--

CREATE TABLE `fan_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `credit_balance` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fan_profiles`
--

INSERT INTO `fan_profiles` (`id`, `user_id`, `phone_no`, `avatar`, `dob`, `credit_balance`, `created_at`, `updated_at`) VALUES
(1, 2, '+987-61-6624099', '0921061967225161572859266.jpg', '1970-01-01', 0, '2019-11-04 01:29:06', '2019-11-04 03:51:06'),
(3, 4, '+983-59-2381520', NULL, '1970-01-01', 0, '2019-11-04 04:46:49', '2019-11-04 04:46:49'),
(4, 5, '+811-19-5411940', NULL, '1996-05-09', 0, '2019-11-04 05:55:53', '2019-11-04 05:55:53'),
(5, 6, '+264-70-2061845', NULL, '2001-11-01', 0, '2019-11-04 05:56:44', '2019-11-04 05:56:44'),
(6, 7, '+375-97-9555895', NULL, '2001-11-01', 0, '2019-11-04 05:58:05', '2019-11-04 05:58:05'),
(8, 9, '+428-47-2239087', '1141417681140251572867701.jpg', '1970-01-01', 0, '2019-11-04 06:11:41', '2019-11-04 06:11:41'),
(9, 10, '+714-82-2648522', '11443814020641911572867878.jpg', '1970-01-01', 0, '2019-11-04 06:14:21', '2019-11-04 06:14:39'),
(10, 11, '+443-83-8923917', '1149265288231521572868166.jpg', '1970-01-01', 0, '2019-11-04 06:19:27', '2019-11-04 06:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `option_name` varchar(255) DEFAULT NULL,
  `option_value` text,
  `autoload` int(11) DEFAULT '1' COMMENT '1=>''yes'',0=>''no''',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option_name`, `option_value`, `autoload`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'Cameo', 1, NULL, '2019-11-02 00:41:18'),
(2, 'admin_mail', 'cameo@mailinator.com', 1, NULL, '2019-11-02 00:41:18'),
(3, 'admin_department', 'CAMEO Host', 1, NULL, '2019-11-02 00:41:18'),
(4, 'ADMIN_PAGE_LIMIT', '10', 1, NULL, '2019-11-02 00:41:18'),
(5, 'gender', '{\"1\":\"Male\",\"2\":\"Female\",\"3\":\"other\"}', 1, NULL, NULL),
(6, 'user', '{\"image\":{\"width\":\"213\",\"height\":\"213\"}}', 1, NULL, NULL),
(7, 'admin', '{\"image\":{\"width\":\"100\",\"height\":\"60\"}}', 1, NULL, NULL),
(8, 'campaign', '{\"image\":{\"width\":\"150\",\"height\":\"150\"}}', 1, NULL, NULL),
(9, 'badge', '{\"image\":{\"width\":\"150\",\"height\":\"150\"}}', 1, NULL, NULL),
(10, 'salutation', '{\"1\":\"Mr\",\"2\":\"Mrs\",\"3\":\"Miss\",\"4\":\"Ms\",\"5\":\"Other\"}', 1, NULL, NULL),
(11, 'rows', '{\"10\":10,\"25\":25,\"50\":50}', 1, NULL, NULL),
(12, 'logo', 'logo.png', 1, NULL, '2018-12-16 20:31:15'),
(14, 'videos', '{\"image\":{\"width\":\"300\",\"height\":\"200\"}}', 1, NULL, NULL),
(16, 'page', '{\"image\":{\"width\":\"1540\",\"height\":\"330\"}}', 1, '2018-12-12 13:00:00', '2018-12-12 13:00:00'),
(17, 'setting', '{\"logo\":{\"width\":\"500\",\"height\":\"395\"}}', 1, '2018-12-16 13:00:00', '2018-12-16 13:00:00'),
(18, 'slider-image', '{\"image\":{\"width\":\"1920\",\"height\":\"1080\"}}', 1, '2019-01-09 13:00:00', '2019-01-09 13:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('testcompany@mailinator.com', '$2y$10$S60F3ndbOrnid7/.J0BB4uStiNCFNEqEkr5eIjEwvmobH9S0OaWI.', '2019-11-01 01:22:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(3) DEFAULT NULL,
  `login_mode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_unique_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(2) NOT NULL,
  `admin_approved` int(2) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `login_mode`, `social_unique_id`, `status`, `admin_approved`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, 1, 1, 'Administrator', 'testcompany@mailinator.com', NULL, '$2y$10$j86QIa3lPL77vlFCOb4RQuo39MQAqEluaKHMrjvWw95eU0r/F.Lou', NULL, '2019-11-01 01:22:13', '2019-11-01 08:22:15'),
(2, 2, NULL, NULL, 1, 1, 'Debra Thornton', 'gytalyviw@yopmail.com', NULL, '$2y$10$jf9fpyA14QPzrLxs9op6V.JjomIrSNpYm5Tw/cpJUNvNgSR4Nl4/O', NULL, '2019-11-04 01:29:06', '2019-11-04 05:10:53'),
(4, 2, NULL, NULL, 1, 1, 'Eve Dawson', 'rikeqy@yopmail.com', NULL, '$2y$10$KLr98Uv3E1Fwn75UorhAC.13q/dKOKPqu6LG8mYBCDVfNP0kXBO7e', NULL, '2019-11-04 04:46:49', '2019-11-04 04:46:49'),
(5, 2, NULL, NULL, 1, 1, 'Heidi Brady', 'dapevigoga@yopmail.com', NULL, '$2y$10$aBFi0kWc5J4JWAPpyv.bJuGeRQyjXXyqiT.sWAbX5agfdR.4v9rmq', NULL, '2019-11-04 05:55:53', '2019-11-04 23:53:23'),
(6, 2, NULL, NULL, 1, 1, 'Karleigh Fowler', 'hoqe@mailinator.com', NULL, '$2y$10$OhUzmLDO4Q7L/7Cg0YLyduMXR9zuxs9A65W7A6MBYQ3lM7ea27EHO', NULL, '2019-11-04 05:56:44', '2019-11-04 05:56:44'),
(7, 2, NULL, NULL, 1, 1, 'Susan Mathews', 'vilit@mailinator.com', NULL, '$2y$10$E8GiOdme9t8uNS/K1Cgreu3wjGrnfhqfEJCSjtjxWS0ea4xrVsVzK', NULL, '2019-11-04 05:58:05', '2019-11-04 05:58:05'),
(9, 2, NULL, NULL, 0, 1, 'Kyle Curtis', 'lalulef@mailinator.com', NULL, '$2y$10$te/55fYEdj2d.3ALFEvVROeW5SCWYWU28a47UE31vMOzCwM/RTHy.', NULL, '2019-11-04 06:11:41', '2019-11-04 06:11:41'),
(10, 2, NULL, NULL, 0, 1, 'Kelsie Garza', 'nuzalasaxe@mailinator.com', NULL, '$2y$10$PfZcABKCQHXSk3mUosL7uOO7nsPGycRoo2bmPD4iHi8iclvwDKuCm', NULL, '2019-11-04 06:14:21', '2019-11-04 06:14:21'),
(11, 2, NULL, NULL, 0, 1, 'Clare Dodson', 'donu@mailinator.com', NULL, '$2y$10$e2.N/3YB8inTto5pl/zoFucQ2toGqKS2PJg9iHse5nErPg1m9YgXG', NULL, '2019-11-04 06:19:27', '2019-11-04 06:19:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fan_profiles`
--
ALTER TABLE `fan_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fan_profiles`
--
ALTER TABLE `fan_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
