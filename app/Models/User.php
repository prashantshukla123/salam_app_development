<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Notifiable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
	
	public $sortable = ['name', 'email', 'status', 'created_at'];
	
	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	/* public function sendPasswordResetNotification($token)
	{
		$this->notify(new \App\Notifications\MailResetPasswordNotification($token));
	} */
	
	public function fanDetails(){
		return $this->hasOne('\App\Models\FanProfile','user_id');
	}
	
	public function artistDetails(){
		return $this->hasOne('\App\Models\ArtistProfile','user_id');
	}
}
