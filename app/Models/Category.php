<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Category extends Authenticatable
{
    use Notifiable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name', 'category_slug', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
	
	public $sortable = ['category_name', 'status', 'created_at'];
	
    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id')->with(['children']);
    }
	
	public static function getList($parent_id, $escpCat = 0,$dotStr = ''){
		$categories = Category::where(['parent_id' => $parent_id])->where('id', '!=', $escpCat)->with(['children'])->get();
		
		$catList = [];
		
		if(!empty($categories)){
			foreach($categories as $k=>$cat){
				$catList[$cat->id] = $dotStr.$cat->category_name;
				
				if($cat->children && $cat->children->count() > 0){
					$catList = childData($cat->children, $escpCat, $catList, $dotStr.'---');
				}
			}
			return $catList;
		}
	}
}
