<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ArtistProfile extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'phone_no', 'avatar', 'dob', 'credit_balance','networkType','social_handle','followers_count','booking_rate','response_time','is_available','self_intro_video','short_description',
    ];
}