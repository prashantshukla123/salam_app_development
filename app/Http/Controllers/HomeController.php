<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
	
	/**
     * Show the application static pages.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function view($slug = null) {
        $page = Page::where('slug', $slug)->where('status', 1);
		
		$page = $page->firstOrFail();
		
		return view('view')->with('page', $page);
    }
	
	public function email()
    {
		$to = 'testsoon@mailinator.com';
		\Mail::send(['html' => 'emails.emailtest'], [], function ($message) use ($to) {
			$message->from('admin@admin.com', 'Apparel Resources Jobs');
			$message->to($to)->subject('Registration successfully');
		});
    }
}
