<?php

namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\ProfileRequest;

use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Category;
use Hash;
use Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
		$this->middleware('auth',['except' => []]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * @url: /admin/dashboard
     */
    public function index(Request $request){
		$limit = 10;
		
		$data = [];
		
		$data['fanCount'] = User::where('role_id' , 2)->count();
		$data['artistCount'] = User::where('role_id' , 3)->count();
		$data['categoryCount'] = Category::count();
		
		return view('sitecontrol/home/index',compact('data','limit'));
	}
	
	/**
     * Show Admin/Agent account details and its form.
     *
     * @return \Illuminate\Http\Response
     * @url: /admin/profile
     */
    public function myaccount(Request $request){
		$user = User::findOrFail(\Auth::user()->id)->first();
		return view('sitecontrol/home/myaccount',compact('user'));
    }
	
	/**
     * Save Admin/Agent account form.
     *
     * @return \Illuminate\Http\Response
     * @url: /admin/profile/save/{id}
     */
	public function saveProfile(ProfileRequest $request){
		$data = $request->all();
		$id = \Auth::user()->id;
		$user = User::where('id',$id)->first();
		
		$success_msg = 'Profile has been Updated Successfully.';
		$error_msg = 'Profile not updated, please try again.';
		$redirect = '/sitecontrol/'.$data['redirectPage'];
		
		//** Below code for set user obj value set**//
		if(isset($data['name']))
			$user->name = $data['name'];
		if(isset($data['email']))
			$user->email = $data['email'];
		
		// Check password change conditions //
		if(isset($data['changePassword'])){
			$user->password = bcrypt($data['password']);
			
			$success_msg = 'Password has been Updated Successfully, Please login again.';
			$error_msg = 'Password not changed successfully, Please try again.';
		}
		
		if($user->save()){
			\Session::flash('success', $success_msg);
			return redirect($redirect);
		}
		else{
			\Session::flash('error', $error_msg);
			return redirect($redirect);
		}
	}
	
	/**
     * Show Change Password form of Admin/Agent.
     *
     * @return \Illuminate\Http\Response
     * @url: /admin/change-password
     */
	public function changepassword(Request $request){
		$user = User::where('id',\Auth::user()->id)->first();
		return view('sitecontrol/home/changepassword',compact('user'));
	}
}
