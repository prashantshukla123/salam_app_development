<?php
namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\EmailRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\EmailTemplate;


class EmailsController extends Controller
{
    public function __construct(){}

	/**
    * Add Email Template function.
    *
    * @return url /admin/emails/add
    */
	public function addForm(Request $request){
		$pageTitle = 'Email Add';
		
		$email = new Email();
		
		return view('Admin/emails/add',compact('pageTitle','email'));
	}
	
	/**
    * Save Email Template function for Add/Edit post form.
    *
    * @return url /admin/emails/save/{id}
    */
	public function saveForm(EmailRequest $request,$id=null){
		$data = $request->all();
		
		$success_msg = 'Successfully Add the email template.';
		$error_msg = 'Something went wrong, the email not add ,please try again.';
		$error_redirect = '/sitecontrol/emails/add';
		
		/** Below code for set email obj accordingly add or edit **/
		if($id){
			$email = EmailTemplate::where('id',$id)->first();
			 
			$success_msg = 'Email content has been Updated Successfully.';
			$error_msg = 'Email content not changed successfully, Please try again.';
			$error_redirect = '/sitecontrol/emails/edit'.$id;
		}else{
			$email = new EmailTemplate();
		}
		
		$email->title = $data['title'];
		$email->slug = str_slug($data['subject'], '-');
		$email->subject = $data['subject'];
		$email->content = $data['content'];
		$email->status = '1';
		if($email->save()){
			\Session::flash('success', $success_msg);
			return \Redirect::to('/sitecontrol/emails/lists');
		}
		else{
			\Session::flash('error', $error_msg);
			return \Redirect::to($error_redirect);
		}
	}
	
	/**
    * List of Email Template function.
    *
    * @return url /admin/emails/lists
    */
	public function lists(Request $request){
		$condition = array();
		$orderBy = array('created_at' => 'DESC');
		$limit = options['ADMIN_PAGE_LIMIT'];
		
		$pageTitle = "Email Template List";
		$data = $request->all();
		if(!$request->isMethod('post') && (!isset($data['page']) && !isset($data['sort']) && !isset($data['direction']))){
			session()->forget('emails');
		}
		
		$orcondition = array();
		$db = EmailTemplate::where($condition);
				
		/* Below code set filter with session */
		if($request->isMethod('post')){
			if(isset($data['f_keyword']) and !empty($data['f_keyword'])){
				session(['emails.f_keyword' => $data['f_keyword']]);
			}else{
				session()->forget('emails.f_keyword');
			}
						
			if(isset($data['rows']) and $data['rows'] != ''){
				session(['emails.rows' => $data['rows']]);
			}else{
				session()->forget('emails.rows');
			}
			
			if(isset($data['f_status']) and $data['f_status'] != ''){
				session(['emails.f_status' => $data['f_status']]);
			}else{
				session()->forget('emails.f_status');
			}
		}
		
		if (session()->has('emails')) {
			if (session()->has('emails.f_keyword')) {
				$f_keyword = session()->get('emails.f_keyword');
				$db->where(function ($q) use($orcondition,$request,$f_keyword) {
					$q->orWhere('title','like','%'.$f_keyword.'%');
					$q->orWhere('subject','like','%'.$f_keyword.'%');
				});
			}
			
			if (session()->has('emails.rows')) {
				$limit = session()->get('emails.rows');
			}
			if (session()->has('emails.f_status')) {
				$status = session()->get('emails.f_status');
				$db->where('status',$status);
			}
		}
		
		if(isset($data['sort']) && isset($data['direction'])){
			$orderBy[$data['sort']] = $data['direction'];
		}
		
		/* End code set filter with session */
		
		$emails = $db->sortable($orderBy)->paginate($limit);
		
		return view('sitecontrol/emails/lists',compact('pageTitle','limit','emails'));
	}
	
	/**
    * View Email Template function.
    *
    * @return url /admin/emails/view/{id}
    */
	public function view(Request $request,$id){
		$pageTitle = 'View Email Template';
		
		$email = EmailTemplate::findOrFail($id);
		
		if(@getimagesize(url('/uploads/logo/'.options['logo']))){
			$url = \URL::to('/uploads/logo/'.options['logo']);
			$email->content = str_replace("##SITELOGO##", $url , $email->content);
		}
			
		return view('sitecontrol/emails/view',compact('pageTitle','email'));
	}
	
	/**
    * Edit Email Template function.
    *
    * @return url /admin/emails/edit
    */
	public function edit(Request $request,$id){
		$pageTitle = 'Edit Email Template';
		
		$email = EmailTemplate::findOrFail($id);
		
		if(@getimagesize(url('/uploads/logo/'.options['logo']))){
			$url = \URL::to('/uploads/logo/'.options['logo']);
			$email->content = str_replace("##SITELOGO##", $url , $email->content);
		}
		
		return view('sitecontrol/emails/edit',compact('pageTitle','email'));
	}
	
	/**
     * Change Email Template Status Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/emails/updatestatus/
     */
	public function updatestatus(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$emailTemplate = EmailTemplate::where('id',$id)->first();
			if (!empty($emailTemplate)) {
				if($emailTemplate->status == 1){
					$emailTemplate->status = 0;
					$currentstatus = 'deactivated';
				}else{
					$emailTemplate->status = 1;
					$currentstatus = 'activated';
				}
				if($emailTemplate->save()){
					\Session::flash('success', 'The email template status has been changed.');
				} else {
					\Session::flash('error', 'The email template status could not be changed. Please, try again.');
				}
			} else {
				\Session::flash('error', 'The email template status could not be changed. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The email template status could not be changed. Please, try again.');
		}
		return redirect('/sitecontrol/emails/lists');
	}
}
