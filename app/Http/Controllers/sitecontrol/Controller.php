<?php

namespace App\Http\Controllers\sitecontrol;

use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	/**
    * Set a global function for send a mail.
    *
    * @return url 
    */
	public function sendMail($params){
		$to = $params['to'];
		$cc = '';
		$pdf = '';
		if(isset($params['cc']) && $params['cc']!=""){
			$cc = $params['cc'];
		}
		
		if(isset($params['pdf']) && $params['pdf']!=""){
			$pdf = $params['pdf'];
		}

		$message_body = '';
		$subject = '';
		
		$data=DB::table('emails')->where('slug', $params['slug'])->get()->toArray();
		
		if(!empty($data)){
			$message_body = $data[0]->message;
			$subject = $data[0]->subject;
		}
		
		foreach($params['params'] as $key=>$val){
			if (strpos($message_body,$key) !== false) {
				$message_body = str_replace($key,$val,$message_body);
				$subject = str_replace($key,$val,$subject);
			}
		}
		
		$message_body = '<div style="font-family:serif;">'.$message_body.'</div>';
		
		\Mail::send([], [], function ($message) use ($to,$cc,$pdf,$data,$subject,$message_body) {
			$message->from(config('constants.ADMIN_MAIL'),config('constants.SITE_NAME') );
			$message->to($to)->subject($subject);
			if($cc!="")
			{
				$message->cc($cc);
			}
			
			if($pdf!="")
			{
				$message->attach($pdf, [
                        'as' => 'Order Receipt.pdf',
                        'mime' => 'application/pdf',
                    ]);
			}

			$message->setBody($message_body, 'text/html');
		});
	}
}
