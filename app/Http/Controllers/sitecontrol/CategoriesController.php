<?php
namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\CategoryRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;


class CategoriesController extends Controller
{
    public function __construct(){}

	/**
    * Add Category function.
    *
    * @return url /sitecontrol/categories/add
    */
	public function addForm(Request $request){
		$pageTitle = 'Add Category';
		
		$categories = Category::getList(0);
		
		$category = new Category();
		
		return view('sitecontrol/categories/add',compact('pageTitle','category','categories'));
	}
	
	/**
    * Save Category function for Add/Edit post form.
    *
    * @return url /sitecontrol/categories/save/{id}
    */
	public function saveForm(CategoryRequest $request,$id=null){
		$data = $request->all();
		
		$success_msg = 'Successfully Add the category.';
		$error_msg = 'Something went wrong, the category not add ,please try again.';
		$error_redirect = '/sitecontrol/categories/add';
		
		/** Below code for set category obj accordingly add or edit **/
		if($id){
			$category = Category::where('id',$id)->first();
			 
			$success_msg = 'Category has been Updated Successfully.';
			$error_msg = 'Category not changed successfully, Please try again.';
			$error_redirect = '/sitecontrol/categories/edit'.$id;
		}else{
			$category = new Category();
		}
		$data['category_slug'] = str_slug($data['category_name'], '-');
		
		$category->category_name = $data['category_name'];
		$category->category_slug = $data['category_slug'];
		
		if(isset($data['parent_id']) && $data['parent_id'] != '')
			$category->parent_id = $data['parent_id'];
		else
			$category->parent_id = 0;
			
		$category->status = 1;
		
		if($category->save()){
			\Session::flash('success', $success_msg);
			return \Redirect::to('/sitecontrol/categories/lists');
		}
		else{
			\Session::flash('error', $error_msg);
			return \Redirect::to($error_redirect);
		}
	}
	
	/**
    * List of Category function.
    *
    * @return url /sitecontrol/categories/lists
    */
	public function lists(Request $request){
		$condition = array('parent_id' => 0);
		$orderBy = array('category_name' => 'ASC');
		$limit = options['ADMIN_PAGE_LIMIT'];
		
		$pageTitle = "Category List";
		
		$data = $request->all();
		if(!$request->isMethod('post') && (!isset($data['page']) && !isset($data['sort']) && !isset($data['direction']))){
			session()->forget('categories');
		}
		
		$orcondition = array();
		$db = Category::where($condition);
		
		/* Below code set filter with session */
		if($request->isMethod('post')){
			if(isset($data['f_keyword']) and !empty($data['f_keyword'])){
				session(['categories.f_keyword' => $data['f_keyword']]);
			}else{
				session()->forget('categories.f_keyword');
			}
						
			if(isset($data['rows']) and $data['rows'] != ''){
				session(['categories.rows' => $data['rows']]);
			}else{
				session()->forget('categories.rows');
			}
			
			if(isset($data['f_status']) and $data['f_status'] != ''){
				session(['categories.f_status' => $data['f_status']]);
			}else{
				session()->forget('categories.f_status');
			}
		}
		
		if (session()->has('categories')) {
			if (session()->has('categories.f_keyword')) {
				$f_keyword = session()->get('categories.f_keyword');
				$db->where(function ($q) use($orcondition,$request,$f_keyword) {
					$q->orWhere('category_name','like','%'.$f_keyword.'%');
				});
			}
			
			if (session()->has('categories.rows')) {
				$limit = session()->get('categories.rows');
			}
			if (session()->has('categories.f_status')) {
				$status = session()->get('categories.f_status');
				$db->where('status',$status);
			}
		}
		
		if(isset($data['sort']) && isset($data['direction'])){
			$orderBy[$data['sort']] = $data['direction'];
		}
		
		$db->with(['children']);
		
		/* End code set filter with session */
		
		$categories = $db->sortable($orderBy)->paginate($limit);
		
		return view('sitecontrol/categories/lists',compact('pageTitle','limit','categories'));
	}
	
	/**
    * View Category function.
    *
    * @return url /sitecontrol/categories/view/{id}
    */
	public function view(Request $request,$id){
		$pageTitle = 'View Category';
		
		$category = Category::findOrFail($id);
		return view('sitecontrol/categories/view',compact('pageTitle','category'));
	}
	
	/**
    * Edit Category function.
    *
    * @return url /sitecontrol/categories/edit
    */
	public function edit(Request $request,$id){
		$pageTitle = 'Edit Category';
		
		$categories = Category::getList(0, $id);
		
		$category = Category::findOrFail($id);
		
		return view('sitecontrol/categories/edit',compact('pageTitle','category','categories'));
	}
	
	/**
     * Delete categories Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/categories/delete/{id}
     */
	public function delete(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$ids[] = $data['id'];
			
			$categories = Category::select('id','parent_id')->where(['parent_id' => $data['id']])->with(['children'])->get();
			
			if(!empty($categories) && $categories->count() > 0){
				foreach($categories as $cat){
					$ids[] = $cat->id;
					
					if(!empty($cat->children) && count($cat->children) > 0)
						$ids = getChildIds($ids,$cat->children);
				}
			}
			
			Category::whereIn('id', $ids)->delete();
			\Session::flash('success', 'Category has been deleted.');
		} else {
			\Session::flash('error', 'Category could not be deleted. Please, try again.');
		}
		return redirect('/sitecontrol/categories/lists');
	}
	
	/**
     * Change Category Status Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/categories/updatestatus/
     */
	public function updatestatus(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$category = Category::where('id',$id)->first();
			if (!empty($category)) {
				if($category->status == 1){
					$category->status = 0;
					$currentstatus = 'deactivated';
				}else{
					$category->status = 1;
					$currentstatus = 'activated';
				}
				if($category->save()){
					\Session::flash('success', 'The category status has been changed.');
				} else {
					\Session::flash('error', 'The category status could not be changed. Please, try again.');
				}
			} else {
				\Session::flash('error', 'The category status could not be changed. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The category status could not be changed. Please, try again.');
		}
		return redirect('/sitecontrol/categories/lists');
	}
	
	public function checkunique(Request $request) {
		if(request()->ajax()){
			$data = $request->all();
			if($data['form_mode'] == 'add') {
				$cat_slug = str_slug($data['category_name']);
				$res = Category::where('category_slug', $cat_slug)->count();
				if(!$res) {
					return response()->json(true);
				}
			} else if($data['form_mode'] == 'edit') {
				$cat_slug = str_slug($data['category_name']);
				$res = Category::where('category_slug', $cat_slug)->where('id', '<>', $data['id'])->count();
				if(!$res) {
					return response()->json(true);
				}
			}
			return response()->json(false);
		}
	}
}
