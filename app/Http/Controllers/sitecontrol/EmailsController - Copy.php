<?php
namespace App\Http\Controllers\Admin;

/** Request File Validations **/
use App\Http\Requests\Admin\EmailRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Email;


class EmailsController extends Controller
{
    public function __construct(){}

	/**
    * Add Email Template function.
    *
    * @return url /admin/emails/add
    */
	public function addForm(Request $request){
		$pageTitle = 'Email Add';
		
		$email = new Email();
		
		return view('Admin/emails/add',compact('pageTitle','email'));
	}
	
	/**
    * Save Email Template function for Add/Edit post form.
    *
    * @return url /admin/emails/save/{id}
    */
	public function saveForm(EmailRequest $request,$id=null){
		$data = $request->all();
		
		$success_msg = 'Successfully Add the email.';
		$error_msg = 'Something went wrong, the email not add ,please try again.';
		$error_redirect = '/admin/emails/add';
		
		/** Below code for set email obj accordingly add or edit **/
		if($id){
			$email = Email::where('id',$id)->first();
			 
			$success_msg = 'Email content has been Updated Successfully.';
			$error_msg = 'Email content not changed successfully, Please try again.';
			$error_redirect = '/admin/emails/edit'.$id;
		}else{
			$email = new Email();
		}
		
		
		$email->slug = $data['slug'];
		$email->subject = $data['subject'];
		$email->message = $data['message'];
		$email->status = '1';
		if($email->save()){
			\Session::flash('success', $success_msg);
			return \Redirect::to('/admin/emails/lists');
		}
		else{
			\Session::flash('error', $error_msg);
			return \Redirect::to($error_redirect);
		}
	}
	
	/**
    * List of Email Template function.
    *
    * @return url /admin/emails/lists
    */
	public function lists(Request $request){
		$pageTitle = 'Emails List';
		
		$limit = options['ADMIN_PAGE_LIMIT'];
		$emails=Email::paginate($limit);
		return view('Admin/emails/lists',compact('pageTitle','limit','emails'));
	}
	
	/**
    * View Email Template function.
    *
    * @return url /admin/emails/view/{id}
    */
	public function view(Request $request,$id){
		$pageTitle = 'Email View';
		
		$email=Email::findOrFail($id);
		return view('Admin/emails/view',compact('pageTitle','email'));
	}
	
	/**
    * Edit Email Template function.
    *
    * @return url /admin/emails/edit
    */
	public function edit(Request $request,$id){
		$pageTitle = 'Content Edit';
		
		$email = Email::findOrFail($id);
		
		return view('Admin/emails/edit',compact('pageTitle','email'));
	}
}
