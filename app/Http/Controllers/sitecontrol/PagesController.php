<?php
namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\PageRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Page;


class PagesController extends Controller
{
    public function __construct(){}

	/**
    * Add Page Template function.
    *
    * @return url /admin/pages/add
    */
	public function addForm(Request $request){
		$pageTitle = 'Add Page';
		
		$page = new Page();
		
		return view('Admin/pages/add',compact('pageTitle','page'));
	}
	
	/**
    * Save Page Template function for Add/Edit post form.
    *
    * @return url /admin/pages/save/{id}
    */
	public function saveForm(PageRequest $request,$id=null){
		$data = $request->all();
		
		$success_msg = 'Successfully Add the page template.';
		$error_msg = 'Something went wrong, the page not add ,please try again.';
		$error_redirect = '/sitecontrol/pages/add';
		
		/** Below code for set page obj accordingly add or edit **/
		if($id){
			$page = Page::where('id',$id)->first();
			 
			$success_msg = 'Page content has been Updated Successfully.';
			$error_msg = 'Page content not changed successfully, Please try again.';
			$error_redirect = '/sitecontrol/pages/edit'.$id;
		}else{
			$page = new Page();
		}
		
		$page->title = $data['title'];
		$page->slug = str_slug($data['title'], '-');
		$page->meta_title = $data['meta_title'];
		$page->meta_keywords = $data['meta_keywords'];
		$page->meta_description = $data['meta_description'];
		$page->content = $data['content'];
		$page->status = '1';
		if($page->save()){
			\Session::flash('success', $success_msg);
			return \Redirect::to('/sitecontrol/pages/lists');
		}
		else{
			\Session::flash('error', $error_msg);
			return \Redirect::to($error_redirect);
		}
	}
	
	/**
    * List of Page Template function.
    *
    * @return url /admin/pages/lists
    */
	public function lists(Request $request){
		$condition = array();
		$orderBy = array('created_at' => 'DESC');
		$limit = options['ADMIN_PAGE_LIMIT'];
		
		$pageTitle = "Page Template List";
		$data = $request->all();
		if(!$request->isMethod('post') && (!isset($data['page']) && !isset($data['sort']) && !isset($data['direction']))){
			session()->forget('pages');
		}
		
		$orcondition = array();
		$db = Page::where($condition);
		
		/* Below code set filter with session */
		if($request->isMethod('post')){
			if(isset($data['f_keyword']) and !empty($data['f_keyword'])){
				session(['pages.f_keyword' => $data['f_keyword']]);
			}else{
				session()->forget('pages.f_keyword');
			}
						
			if(isset($data['rows']) and $data['rows'] != ''){
				session(['pages.rows' => $data['rows']]);
			}else{
				session()->forget('pages.rows');
			}
			
			if(isset($data['f_status']) and $data['f_status'] != ''){
				session(['pages.f_status' => $data['f_status']]);
			}else{
				session()->forget('pages.f_status');
			}
		}
		
		if (session()->has('pages')) {
			if (session()->has('pages.f_keyword')) {
				$f_keyword = session()->get('pages.f_keyword');
				$db->where(function ($q) use($orcondition,$request,$f_keyword) {
					$q->orWhere('title','like','%'.$f_keyword.'%');
				});
			}
			
			if (session()->has('pages.rows')) {
				$limit = session()->get('pages.rows');
			}
			if (session()->has('pages.f_status')) {
				$status = session()->get('pages.f_status');
				$db->where('status',$status);
			}
		}
		
		if(isset($data['sort']) && isset($data['direction'])){
			$orderBy[$data['sort']] = $data['direction'];
		}
		
		/* End code set filter with session */
		
		$pages = $db->sortable($orderBy)->paginate($limit);
		
		return view('sitecontrol/pages/lists',compact('pageTitle','limit','pages'));
	}
	
	/**
    * View Page Template function.
    *
    * @return url /admin/pages/view/{id}
    */
	public function view(Request $request,$id){
		$pageTitle = 'View Page Template';
		
		$page = Page::findOrFail($id);
		
		return view('sitecontrol/pages/view',compact('pageTitle','page'));
	}
	
	/**
    * Edit Page Template function.
    *
    * @return url /admin/pages/edit
    */
	public function edit(Request $request,$id){
		$pageTitle = 'Edit Page Template';
		
		$page = Page::findOrFail($id);
		
		return view('sitecontrol/pages/edit',compact('pageTitle','page'));
	}
	
	/**
     * Change Page Template Status Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/pages/updatestatus/
     */
	public function updatestatus(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$pageTemplate = Page::where('id',$id)->first();
			if (!empty($pageTemplate)) {
				if($pageTemplate->status == 1){
					$pageTemplate->status = 0;
					$currentstatus = 'deactivated';
				}else{
					$pageTemplate->status = 1;
					$currentstatus = 'activated';
				}
				if($pageTemplate->save()){
					\Session::flash('success', 'The page template status has been changed.');
				} else {
					\Session::flash('error', 'The page template status could not be changed. Please, try again.');
				}
			} else {
				\Session::flash('error', 'The page template status could not be changed. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The page template status could not be changed. Please, try again.');
		}
		return redirect('/sitecontrol/pages/lists');
	}
}
