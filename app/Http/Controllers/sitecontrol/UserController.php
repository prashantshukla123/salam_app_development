<?php

namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\UserRequest;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\FanProfile;
use App\Models\EmailTemplate;
use App\Models\Option;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Image;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Show the application admin login form.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/login
     */
	public function login(Request $request){
		$pageTitle = ((!empty($options_array['site_name'])) ? $options_array['site_name'] : env('APP_NAME')).' Login';
		
		if(isset(\Auth::user()->id)){
			return redirect('/sitecontrol/dashboard');
		}
		
		$options = Option::where('autoload',1)->get();
		$options_array = array();
		foreach($options as $val){
			$options_array[$val->option_name] = $val->option_value;
		}
		
		$remember = Cache::get('remmember_login');
		
		if($request->isMethod('post')) { 
			$data =$request->all();
			$credentials = ['email' => $data['Username'],'password' => $data['Password']];
			if( Auth::attempt($credentials)){
				if(Auth::user()->status == 1){
					if(isset($data['remember_me']) && $data['remember_me'] == 1){
						Cache::put('remmember_login', $data,(2*(24*60)));
					}else{
						\Cache::forget('remmember_login');
					}
					
					\Session::flash('success', 'you have successful login.');
					return \Redirect::to('/sitecontrol/dashboard');
				}else{
					Auth::logout();
					\Session::flash('error', "User doesn't exist.");
					return \Redirect::to('/sitecontrol/login');
				}
			}else{
				\Session::flash('error', 'Email and password incorrect.');
				return \Redirect::to('/sitecontrol/login');
			}
		} 
		return view('sitecontrol/users/login',compact('pageTitle','options_array','remember'));
    }
	
	/**
     * Add User Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/add
     */
	public function addForm(Request $request){
		$pageTitle = 'Add fan';
		
		$user = new User();
		return view('sitecontrol/users/add',compact('pageTitle','user'));
	}
	
	/**
     * Save User Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/save/{id}
     */
	public function saveForm(UserRequest $request,$id=null){
		$data = $request->all();
		
		$success_msg = 'User Add successfully.';
		$error_msg = 'User not Add, please try again.';
		$error_redirect = '/sitecontrol/users/add';
		
		/** Below code for set user obj accordingly add or edit **/
		
		if($id){
			$task_type = 'edit';
			$user = User::where('id',$id)->first();
			$mail_slug = '';
			if(!empty($data['password'])){
				$password = bcrypt($data['password']);
			}else{
				$password = $user->password;
			}
			
			$success_msg = 'User has been Updated Successfully.';
			$error_msg = 'User not edited, please try again.';
			$error_redirect = '/sitecontrol/users/edit'.$id;
		}else{
			$task_type = 'add';
			$user = new User();
			$mail_slug = 'admin_register';
			$password = bcrypt($data['password']);
		}
		
		/** Below code for save image **/
		$destinationPath = public_path('/uploads/users/');
		$newName = '';
		if ($request->hasFile('image')) {
			$fileName = $data['image']->getClientOriginalName();
			$file = request()->file('image');
			$fileNameArr = explode('.', $fileName);
			$fileNameExt = end($fileNameArr);
			$newName = date('His').rand() . time() . '.' . $fileNameExt;
			
			$file->move($destinationPath, $newName);
			
			$user_config = json_decode(options['user'],true);
			
			$img = Image::make(public_path('/uploads/users/'.$newName));
			
			$img->resize($user_config['image']['width'], $user_config['image']['height'], function($constraint) {
				$constraint->aspectRatio();
			});
			
			$img->save(public_path('/uploads/users/thumb/'.$newName));
			
			//** Below code for unlink old image **//
			$oldImage = public_path('/uploads/users/'.$user->image);
			$oldImageThumb = public_path('/uploads/users/thumb/'.$user->image);
			if(!empty($user->image) && @getimagesize($oldImage) && file_exists($oldImage)) {
				unlink($oldImage);
				unlink($oldImageThumb);
			}
		}
		
		//** Below code for set user obj value set**//
		$user->role_id = 2;
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->username = $data['username'];
		$user->password = $password;
		$user->admin_approved = 1;
		$user->status = $data['status'];
		if($user->save()){
			$fan = FanProfile::where(['user_id' => $user->id])->first();
			if(!$fan){
				$fan = new FanProfile();
				$fan->user_id = $user->id;
			}
			
			$fan->dob = date('Y-m-d',strtotime($data['fanDetails']['dob']));
			if(!empty($newName))
				$fan->avatar = $newName;
			
			$fan->save();
			
			//Send mail
			if($task_type == 'add') {
				$mailTemplate = EmailTemplate::where('id',3)->first();
				if(!empty($mailTemplate) && $mailTemplate->content != ''){
					$mail_from_email = options['mail_from_email'];
					$mail_from_name = options['mail_from_name'];
					$admin_name = options['admin_name'];
					$sitename = options['site_name'];
					$site_footer = options['site_footer'];
					$to = $user->email;
					
					$mailContent = $mailTemplate->content;
					$mailContent = str_replace("##NAME##", $user->name, $mailContent);
					$mailContent = str_replace("##EMAIL##", $user->email, $mailContent);
					$mailContent = str_replace("##PASSWORD##", $data['password'], $mailContent);
					$mailContent = str_replace("##LOGINURL##", url('/login'), $mailContent);
					$mailContent = str_replace("##SITEURL##", url('/'), $mailContent);
					$mailContent = str_replace("##SITENAME##", $sitename, $mailContent);
					$mailContent = str_replace("##COPYRIGHT##", $site_footer, $mailContent);
					
					\Mail::send(['html' => 'emails.emailTemplate'], ['content' => $mailContent], function ($message) use ($to, $mail_from_email, $mail_from_name, $mailTemplate, $mailContent) {
						$message->from($mail_from_email, $mail_from_name);
						$message->to($to)->subject($mailTemplate->subject);
					});
				}
			}
			//Send mail
			
			\Session::flash('success', $success_msg);
			return redirect('/sitecontrol/users/lists');
		}
		else{
			\Session::flash('error', $error_msg);
			return redirect($error_redirect);
		}
	}
	
	/**
     * Show list of User (Artist / Fan) Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/lists
     */
	public function lists(Request $request){
		
		$orderBy = array('created_at' => 'DESC');
		$limit = options['ADMIN_PAGE_LIMIT'];
		$user_type = '';
		
		$pageTitle = "Users List";
		
		$data = $request->all();
		
		if(!$request->isMethod('post') && (!isset($data['page']) && !isset($data['sort']) && !isset($data['direction']))){
			session()->forget('users');
		}
		
		$orcondition = array();
		$db = User::where('id', '!=', 0);
		
		/* Below code set filter with session */
		if($request->isMethod('post')){
			if(isset($data['f_keyword']) and !empty($data['f_keyword'])){
				session(['users.f_keyword' => $data['f_keyword']]);
			}else{
				session()->forget('users.f_keyword');
			}
						
			if(isset($data['rows']) and $data['rows'] != ''){
				session(['users.rows' => $data['rows']]);
			}else{
				session()->forget('users.rows');
			}
						
			if(isset($data['user_type'])){
				session(['users.user_type' => $data['user_type']]);
			}else{
				session()->forget('users.user_type');
			}
			
			if(isset($data['f_status']) and $data['f_status'] != ''){
				session(['users.f_status' => $data['f_status']]);
			}else{
				session()->forget('users.f_status');
			}
		}else{
			if(isset($data['user_type']) and $data['user_type'] != ''){
				$user_type = $data['user_type'];
			}
		}
		
		if (session()->has('users')) {
			if (session()->has('users.f_keyword')) {
				$f_keyword = session()->get('users.f_keyword');
				$db->where(function ($q) use($orcondition,$request,$f_keyword) {
					$q->orWhere('name','like','%'.$f_keyword.'%');
					$q->orWhere('email','like','%'.$f_keyword.'%');
				});
			}
			
			if (session()->has('users.rows')) {
				$limit = session()->get('users.rows');
			}
			
			if (session()->has('users.user_type')) {
				$user_type = session()->get('users.user_type');
				if(!empty($user_type))
					$db->where('role_id',$user_type);
				else
					$db->whereIn('role_id', [2,3]);
			}
			
			if (session()->has('users.f_status')) {
				$status = session()->get('users.f_status');
				$db->where('status',$status);
			}
		}else{
			if(!empty($user_type))
				$db->where('role_id',$user_type);
			else
				$db->whereIn('role_id', [2,3]);
		}
		
		if(isset($data['sort']) && isset($data['direction'])){
			$orderBy[$data['sort']] = $data['direction'];
		}
		
		/* End code set filter with session */
		
		$db->with(['fanDetails','artistDetails']);
		
		$users = $db->sortable($orderBy)->paginate($limit);
		
		return view('sitecontrol/users/lists',compact('pageTitle','limit','users','user_type'));
	}
	
	/**
     * Edit User Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/edit
     */
	public function edit(Request $request,$id){
		$pageTitle = "Fan Edit";
		
		$user = User::with(['fanDetails'])->findOrFail($id);
		
		return view('sitecontrol/users/edit',compact('pageTitle','user','id'));
	}
	
	/**
     * View User Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/view/{id}
     */
	public function view($id){
		$pageTitle = "Fan Details";
		$user = User::where('id', $id)->with(['fanDetails'])->get()->toArray();
		//pr($user);die;
		return view('sitecontrol/users/view',compact('pageTitle','user'));
	}

	/**
     * Delete User Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/delete/{id}
     */
	public function delete(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$user = User::where('id', '=', $id)->with('fanDetails')->first()->toArray();
			if (!empty($user)) {
				User::where('id', $id)->delete();
				FanProfile::where('user_id',$id)->delete();
				
				$avatar_path = public_path() . DIRECTORY_SEPARATOR  . 'uploads' . DIRECTORY_SEPARATOR  . 'users';
				$filePath = $avatar_path . DIRECTORY_SEPARATOR . $user['fan_details']['avatar'];
				$thumbFilePath = $avatar_path . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $user['fan_details']['avatar'];
				if($user['fan_details']['avatar'] != '' && file_exists($avatar_path)) {
					@unlink($filePath);
					@unlink($thumbFilePath);
				}
				
				\Session::flash('success', 'The user has been deleted.');
			} else {
				\Session::flash('error', 'The user could not be deleted. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The user could not be deleted. Please, try again.');
		}
		return redirect('/sitecontrol/users/lists');
	}
	
	/**
     * Change User Status Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/users/updatestatus/
     */
	public function updatestatus(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$user = User::where('id',$id)->first();
			if (!empty($user)) {
				if($user->status == 1){
					$user->status = 0;
					$currentstatus = 'deactivated';
				}else{
					$user->status = 1;
					$user->admin_approved = 1;
					$currentstatus = 'activated';
				}
				if($user->save()){
					\Session::flash('success', 'The user status has been changed.');
				} else {
					\Session::flash('error', 'The user status could not be changed. Please, try again.');
				}
			} else {
				\Session::flash('error', 'The user status could not be changed. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The user status could not be changed. Please, try again.');
		}
		return redirect('/sitecontrol/users/lists');
	}
	
	public function deleteImage(Request $request){
		if($request->isMethod('post')){
			$data = $request->all();
			
			DB::table($data['table'])->where('user_id', $data['id'])->update(['avatar' => null]);
		
			//** Below code for unlink old image **//
			$oldImage = public_path($data['path'].$data['name']);
			$oldImageThumb = public_path($data['path'].'thumb/'.$data['name']);
			if(!empty($data['name']) && @getimagesize($oldImage) && file_exists($oldImage)) {
				unlink($oldImage);
				unlink($oldImageThumb);
			}
			
			return json_encode(['status' => true]);
		}
	}
	
	/**
	* @return \Illuminate\Support\Collection
    */
    public function export() {
        return Excel::download(new UserExport, 'users.xlsx');
    }
}