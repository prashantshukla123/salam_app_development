<?php
namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\SettingRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Option;

use Image; 

class SettingsController extends Controller
{
    public function __construct(){}

	/**
     * Show Admin Setting Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /admin/setting/admin
     */
	public function admin(Request $request){
		$pageTitle = 'Set Admin Setting';
		
		$options = Option::pluck('option_value','option_name');
		
		return view('sitecontrol/settings/admin',compact('pageTitle','options'));
	}
	
	/**
     * Save admin setting details by form post.
     *
     * @return \Illuminate\Http\Response
     * @url: /admin/setting/admin  :(POST URL)
     */
	public function saveAdminForm(SettingRequest $request,$id=null){
		$data = $request->all();
		
		$success_msg = 'Successfully Updated Settings.';
		$error_msg = 'Something went wrong, the settings not updated ,please try again.';
		$error_redirect = '/sitecontrol/setting/admin';
		
		/** Below code for save image **/
		$destinationPath = public_path('/uploads/logo/');
		$newName = '';
		if ($request->hasFile('logo')) {
			$fileName = $data['logo']->getClientOriginalName();
			$file = request()->file('logo');
			$fileNameArr = explode('.', $fileName);
			$fileNameExt = end($fileNameArr);
			$newName = 'logo.' . $fileNameExt;
			
			$file->move($destinationPath, $newName);
			
			$setting_config = json_decode(options['setting'],true);
			
			$img = Image::make(public_path('/uploads/logo/'.$newName));
			$img->resize($setting_config['logo']['width'],$setting_config['logo']['height']);
			$img->save(public_path('/uploads/logo/'.$newName));
		}
		
		if(isset($data['site_name']))
			Option::where('option_name','site_name')->update(['option_value'=>$data['site_name']]);
		
		if(isset($data['admin_mail']))
			Option::where('option_name','admin_mail')->update(['option_value'=>$data['admin_mail']]);
		
		if(isset($data['admin_department']))
			Option::where('option_name','admin_department')->update(['option_value'=>$data['admin_department']]);
		
		if(isset($data['ADMIN_PAGE_LIMIT']))
			Option::where('option_name','ADMIN_PAGE_LIMIT')->update(['option_value'=>$data['ADMIN_PAGE_LIMIT']]);
		
		if(!empty($newName))
			Option::where('option_name','logo')->update(['option_value'=>$newName]);
		
		\Cache::forget('options');
		
		\Session::flash('success', $success_msg);
		return \Redirect::to('/sitecontrol/setting/admin');
	}	

}
