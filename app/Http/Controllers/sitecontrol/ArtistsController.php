<?php

namespace App\Http\Controllers\sitecontrol;

/** Request File Validations **/
use App\Http\Requests\Admin\ArtistRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\ArtistProfile;
use App\Models\EmailTemplate;

use Image;

class ArtistsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

	/**
     * Add Artist Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/add
     */
	public function addForm(Request $request){
		$pageTitle = 'Add artist';
		
		$user = new User();
		return view('sitecontrol/artists/add',compact('pageTitle','user'));
	}
	
	/**
     * Save Artist Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/save/{id}
     */
	public function saveForm(ArtistRequest $request,$id=null){
		$data = $request->all();
		//pr($data);die;
		
		if(!array_key_exists('is_available',$data['artistDetails']))
			$data['artistDetails']['is_available'] = 0;
		
		
		$success_msg = 'Artist Add successfully.';
		$error_msg = 'Artist not Add, please try again.';
		$error_redirect = '/sitecontrol/artists/add';
		
		/** Below code for set user obj accordingly add or edit **/
		
		if($id){
			$task_type = 'edit';
			$user = User::where('id',$id)->first();
			$mail_slug = '';
			if(!empty($data['password'])){
				$password = bcrypt($data['password']);
			}else{
				$password = $user->password;
			}
			
			$success_msg = 'Artist has been Updated Successfully.';
			$error_msg = 'Artist not edited, please try again.';
			$error_redirect = '/sitecontrol/artists/edit'.$id;
		}else{
			$task_type = 'add';
			$user = new User();
			$mail_slug = 'admin_register';
			$password = bcrypt($data['password']);
		}
		
		/** Below code for save image **/
		$destinationPath = public_path('/uploads/artists/');
		$newName = '';
		if ($request->hasFile('image')) {
			$fileName = $data['image']->getClientOriginalName();
			$file = request()->file('image');
			$fileNameArr = explode('.', $fileName);
			$fileNameExt = end($fileNameArr);
			$newName = date('His').rand() . time() . '.' . $fileNameExt;
			
			$file->move($destinationPath, $newName);
			
			$user_config = json_decode(options['user'],true);
			
			$img = Image::make(public_path('/uploads/artists/'.$newName));
			
			$img->resize($user_config['image']['width'], $user_config['image']['height'], function($constraint) {
				$constraint->aspectRatio();
			});
			
			$img->save(public_path('/uploads/artists/thumb/'.$newName));
			
			//** Below code for unlink old image **//
			$oldImage = public_path('/uploads/artists/'.$user->image);
			$oldImageThumb = public_path('/uploads/artists/thumb/'.$user->image);
			if(!empty($user->image) && @getimagesize($oldImage) && file_exists($oldImage)) {
				unlink($oldImage);
				unlink($oldImageThumb);
			}
		}
		
		//** Below code for set user obj value set**//
		$user->role_id = 3;
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->username = $data['username'];
		$user->password = $password;
		$user->admin_approved = 1;
		$user->is_artist = 1;
		$user->status = $data['status'];
		if($user->save()){
			$artist = ArtistProfile::where(['user_id' => $user->id])->first();
			if(!$artist){
				$artist = new ArtistProfile();
				$artist->user_id = $user->id;
			}
			
			$artist->dob = date('Y-m-d',strtotime($data['artistDetails']['dob']));
			
			if(!empty($newName))
				$artist->avatar = $newName;
				
			$artist->networkType = $data['artistDetails']['networkType'];
			$artist->social_handle = $data['artistDetails']['social_handle'];
			$artist->followers_count = $data['artistDetails']['followers_count'];
			$artist->booking_rate = $data['artistDetails']['booking_rate'];
			$artist->response_time = $data['artistDetails']['response_time'];
			$artist->is_available = $data['artistDetails']['is_available'];
			
			if(!empty($data['artistDetails']['self_intro_video_file_path']) && !empty($data['artistDetails']['self_intro_video_file_name'])){
				$videoTempPath = storage_path("app/".$data['artistDetails']['self_intro_video_file_path'].'/'.$data['artistDetails']['self_intro_video_file_name']);
				
				$videoName = explode('____',$data['artistDetails']['self_intro_video_file_name']);
				
				$videoOrgPath = public_path('/uploads/artists/videos/'.$videoName[1]);
				
				
				if (!copy($videoTempPath, $videoOrgPath)) {
					if (copy($videoTempPath, $videoOrgPath)) {
						unlink($videoTempPath);
					}
				}else{
					unlink($videoTempPath);
				}
				
				//** Below code for unlink old video **//
				
				$oldVideo = public_path('/uploads/artists/videos/'.$artist->self_intro_video);
				if(!empty($artist->self_intro_video) && file_exists($oldVideo)) {
					unlink($oldVideo);
				}
				
				$artist->self_intro_video = $videoName[1];
			}
			
			$artist->short_description = $data['artistDetails']['short_description'];
			
			$artist->save();
			
			//Send mail
			if($task_type == 'add') {
				$mailTemplate = EmailTemplate::where('id',3)->first();
				if(!empty($mailTemplate) && $mailTemplate->content != ''){
					$mail_from_email = options['mail_from_email'];
					$mail_from_name = options['mail_from_name'];
					$admin_name = options['admin_name'];
					$sitename = options['site_name'];
					$site_footer = options['site_footer'];
					$to = $user->email;
					
					$mailContent = $mailTemplate->content;
					$mailContent = str_replace("##NAME##", $user->name, $mailContent);
					$mailContent = str_replace("##EMAIL##", $user->email, $mailContent);
					$mailContent = str_replace("##PASSWORD##", $data['password'], $mailContent);
					$mailContent = str_replace("##LOGINURL##", url('/login'), $mailContent);
					$mailContent = str_replace("##SITEURL##", url('/'), $mailContent);
					$mailContent = str_replace("##SITENAME##", $sitename, $mailContent);
					$mailContent = str_replace("##COPYRIGHT##", $site_footer, $mailContent);
					
					\Mail::send(['html' => 'emails.emailTemplate'], ['content' => $mailContent], function ($message) use ($to, $mail_from_email, $mail_from_name, $mailTemplate, $mailContent) {
						$message->from($mail_from_email, $mail_from_name);
						$message->to($to)->subject($mailTemplate->subject);
					});
				}
			}
			//Send mail
			
			\Session::flash('success', $success_msg);
			return redirect('/sitecontrol/users/lists?user_type=3');
		}
		else{
			\Session::flash('error', $error_msg);
			return redirect($error_redirect);
		}
	}
	
	/**
     * Show list of Artists Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/lists
     */
	public function lists(Request $request){
		
		$condition['role_id'] = 3;
		$orderBy = array('created_at' => 'DESC');
		$limit = options['ADMIN_PAGE_LIMIT'];
		
		$pageTitle = "Artists List";
		
		$data = $request->all();
		if(!$request->isMethod('post') && (!isset($data['page']) && !isset($data['sort']) && !isset($data['direction']))){
			session()->forget('artists');
		}
		
		$orcondition = array();
		$db = User::where($condition);
		
		/* Below code set filter with session */
		if($request->isMethod('post')){
			if(isset($data['f_keyword']) and !empty($data['f_keyword'])){
				session(['artists.f_keyword' => $data['f_keyword']]);
			}else{
				session()->forget('artists.f_keyword');
			}
						
			if(isset($data['rows']) and $data['rows'] != ''){
				session(['artists.rows' => $data['rows']]);
			}else{
				session()->forget('artists.rows');
			}
			
			if(isset($data['f_status']) and $data['f_status'] != ''){
				session(['artists.f_status' => $data['f_status']]);
			}else{
				session()->forget('artists.f_status');
			}
		}
		
		if (session()->has('artists')) {
			if (session()->has('artists.f_keyword')) {
				$f_keyword = session()->get('artists.f_keyword');
				$db->where(function ($q) use($orcondition,$request,$f_keyword) {
					$q->orWhere('name','like','%'.$f_keyword.'%');
					$q->orWhere('email','like','%'.$f_keyword.'%');
				});
			}
			
			if (session()->has('artists.rows')) {
				$limit = session()->get('artists.rows');
			}
			if (session()->has('artists.f_status')) {
				$status = session()->get('artists.f_status');
				$db->where('status',$status);
			}
		}
		
		if(isset($data['sort']) && isset($data['direction'])){
			$orderBy[$data['sort']] = $data['direction'];
		}
		
		/* End code set filter with session */
		
		$db->with('artistDetails');
		
		$artists = $db->sortable($orderBy)->paginate($limit);
		
		return view('sitecontrol/artists/lists',compact('pageTitle','limit','artists'));
	}
	
	/**
     * Edit Artist Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/edit
     */
	public function edit(Request $request,$id){
		$pageTitle = "Artists Edit";
		
		$user = User::with(['artistDetails'])->findOrFail($id);
		
		return view('sitecontrol/artists/edit',compact('pageTitle','user','id'));
	}
	
	/**
     * View Artist Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/view/{id}
     */
	public function view($id){
		$pageTitle = "Artist Details";
		$user = User::where('id', $id)->with(['artistDetails'])->get()->toArray();
		//pr($user);die;
		
		return view('sitecontrol/artists/view',compact('pageTitle','user'));
	}

	/**
     * Delete Artist Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/delete/{id}
     */
	public function delete(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$user = User::where('id', '=', $id)->with('artistDetails')->first()->toArray();
			if (!empty($user)) {
				User::where('id', $id)->delete();
				ArtistProfile::where('user_id',$id)->delete();
				
				$avatar_path = public_path() . DIRECTORY_SEPARATOR  . 'uploads' . DIRECTORY_SEPARATOR  . 'artists';
				$filePath = $avatar_path . DIRECTORY_SEPARATOR . $user['artist_details']['avatar'];
				$thumbFilePath = $avatar_path . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $user['artist_details']['avatar'];
				if($user['artist_details']['avatar'] != '' && file_exists($filePath)) {
					@unlink($filePath);
					@unlink($thumbFilePath);
				}
				$videoPath = $avatar_path . DIRECTORY_SEPARATOR . $user['artist_details']['self_intro_video'];
				if($user['artist_details']['self_intro_video'] != '' && file_exists($videoPath)) {
					@unlink($videoPath);
				}
				
				\Session::flash('success', 'The user has been deleted.');
			} else {
				\Session::flash('error', 'The user could not be deleted. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The user could not be deleted. Please, try again.');
		}
		return redirect('/sitecontrol/users/lists?user_type=3');
	}
	
	/**
     * Change Artist Status Function.
     *
     * @return \Illuminate\Http\Response
     * @url: /sitecontrol/artists/updatestatus/
     */
	public function updatestatus(Request $request){
		$data = $request->all();
		if(isset($data['id']) && !empty($data['id'])) {
			$id = $data['id'];
			$user = User::where('id',$id)->first();
			if (!empty($user)) {
				if($user->status == 1){
					$user->status = 0;
					$currentstatus = 'deactivated';
				}else{
					$user->status = 1;
					$user->admin_approved = 1;
					$currentstatus = 'activated';
				}
				if($user->save()){
					\Session::flash('success', 'The user status has been changed.');
				} else {
					\Session::flash('error', 'The user status could not be changed. Please, try again.');
				}
			} else {
				\Session::flash('error', 'The user status could not be changed. Please, try again.');
			}
		} else {
			\Session::flash('error', 'The user status could not be changed. Please, try again.');
		}
		return redirect('/sitecontrol/users/lists?user_type=3');
	}
}
