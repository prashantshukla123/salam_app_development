<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		
        $validate = [
            'salutation' => 'sometimes|required',
            'fname' => 'sometimes|required|string|max:20|regex:/^[a-zA-Z\\s]+$/u',
			'lname' => 'sometimes|required|string|max:20|regex:/^[a-zA-Z\\s]+$/u',
			'username' => 'sometimes|required|string|max:255',
			'dob' => 'sometimes|required',
			'gender' => 'sometimes|required',
			'designation' => 'sometimes|required',
			'department' => 'sometimes|required',
			'city' => 'sometimes|required',
			'country' => 'sometimes|required',
			'sso_id' => 'sometimes|required',
			'phone_number' => 'sometimes|required',
        ];
		
		if(isset($data['id'])){
			$validate['email'] = 'sometimes|required|string|email|max:50|unique:users,email,'.$data['id'];
			if(!empty($data['password']))
				$validate['password'] = 'sometimes|required|string|min:6';
			if(!empty($data['image']))
				$validate['image'] = 'sometimes|required|image|mimes:jpeg,jpg,png,gif|max:512';
		}else{
			$validate['email'] = 'sometimes|required|string|email|max:50|unique:users';
			$validate['password'] = 'sometimes|required|string|min:6';
			$validate['image'] = 'sometimes|required';
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			
		];
	}
}
