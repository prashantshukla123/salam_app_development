<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		
        if(isset($data['id'])){
			$validate['category_name'] = 'required|string|sanitizeScripts|min:3|max:100|unique:categories,category_name,'.$data['id'];
			$validate['category_slug'] = 'required|string|min:3|max:100|unique:categories,category_slug,'.$data['id'];
		}else{
			$validate['category_name'] = 'required|string|sanitizeScripts|min:3|max:100|unique:categories';
			$validate['category_slug'] = 'required|string|min:3|max:100|unique:categories';
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			'category_name.sanitize_scripts' => 'Script tags are not allowed for this field.',
		];
	}
}
