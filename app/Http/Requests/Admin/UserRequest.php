<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		//pr($data);die;
        $validate = [
            'name' => 'sometimes|required|string|sanitizeScripts|max:20|regex:/^[a-zA-Z\\s]+$/u',
			'dob' => 'sometimes|required',
        ];
		if(isset($data['id'])){
			$validate['email'] = 'sometimes|required|string|email|max:50|unique:users,email,'.$data['id'];
			
			$validate['username'] = 'sometimes|required|sanitizeScripts|unique:users,username,'.$data['id'];
			
			if(!empty($data['password']))
				$validate['password'] = 'sometimes|required|string|min:6|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/';
			if(!empty($data['image']))
				$validate['edit_image'] = 'sometimes|required';
		}else{
			$validate['email'] = 'sometimes|required|string|email|max:50|unique:users';
			$validate['username'] = 'sometimes|required|sanitizeScripts|unique:users';
			$validate['password'] = 'sometimes|required|string|min:6|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/';
			$validate['image'] = 'sometimes|required|mimes:jpeg,jpg,gif,png|max:5120';
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			'name.regex' => 'Special character not allow.',
			'name.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'phone_no.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'password.regex' => 'Password should contain one capital letter, one special letter and one number.',
			'image.mimes' => 'Please upload a valid image.',
			'image.max' => 'The image must be less than 5 MB.',
		];
	}
}
