<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		
        $validate = [
            'content' => 'required|string|sanitizeScripts',
			'meta_title' => 'required|string|sanitizeScripts',
			'meta_keywords' => 'required|string|sanitizeScripts',
			'meta_description' => 'required|string|sanitizeScripts',
        ];
		if(isset($data['id'])){
			$validate['title'] = 'required|string|sanitizeScripts|unique:pages,title,'.$data['id'];
			$validate['slug'] = 'required|string|unique:pages,slug,'.$data['id'];
		}else{
			$validate['title'] = 'required|string|unique:pages';
			$validate['slug'] = 'required|string|unique:pages';
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			'title.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'content.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'meta_title.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'meta_keywords.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'meta_description.sanitize_scripts' => 'Script tags are not allowed for this field.',
		];
	}
}