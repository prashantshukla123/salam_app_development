<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

use DB;
use App\Models\Question;
use App\Models\CompanyContact;

class SettingRequest extends FormRequest
{
	/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$this->data = $request->all();
		//pr($this->data);die;
        $validate = [
            'site_name' => 'sometimes|required|sanitizeScripts',
            'admin_mail' => 'sometimes|required|string|sanitizeScripts|email|max:255',
            'admin_department' => 'sometimes|required|sanitizeScripts',
            'ADMIN_PAGE_LIMIT' => 'sometimes|required|sanitizeScripts',
            'name' => 'sometimes|required|sanitizeScripts|max:80',
            'iso' => 'sometimes|required|sanitizeScripts|max:2',
            'phonecode' => 'sometimes|required|sanitizeScripts|max:5',
        ];
		
		return $validate;
    }
	
	public function withValidator($validator)
	{
		$validator->after(function ($validator) {
			if(isset($this->data['lightningBadgeTime_hour']) && isset($this->data['lightningBadgeTime_minute']) && isset($this->data['lightningBadgeTime_second'])){
				/* Below code for validate badge time */
				
				$check = 1;
				if($this->data['lightningBadgeTime_hour'] < 1 && $this->data['lightningBadgeTime_minute'] < 1 && $this->data['lightningBadgeTime_second'] < 1){
					$check = 0;
					$errorMsg = 'Something is wrong with this field!,Please select time more than 1sec';
				}
				
				if (!$check) {
					$validator->errors()->add('lightningBadgeTime_time', $errorMsg);
				}
				
				/* End code for validate video pause time */
			}
		});
	}
	
	public function messages(){
		return [
			'site_name.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'admin_mail.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'admin_department.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'ADMIN_PAGE_LIMIT.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'name.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'iso.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'phonecode.sanitize_scripts' => 'Script tags are not allowed for this field.',
		];
	}	
}
