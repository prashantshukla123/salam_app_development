<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		
        $validate = [
            'content' => 'required|string|sanitizeScripts'
        ];
		if(isset($data['id'])){
			$validate['title'] = 'required|string|sanitizeScripts|unique:email_templates,title,'.$data['id'];
			$validate['slug'] = 'required|string|unique:email_templates,slug,'.$data['id'];
			$validate['subject'] = 'required|string|sanitizeScripts|unique:email_templates,subject,'.$data['id'];
		}else{
			$validate['title'] = 'required|string|sanitizeScripts|unique:email_templates';
			$validate['slug'] = 'required|string|unique:email_templates';
			$validate['subject'] = 'required|string|sanitizeScripts|unique:email_templates';
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			'title.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'subject.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'content.sanitize_scripts' => 'Script tags are not allowed for this field.',
		];
	}
}
