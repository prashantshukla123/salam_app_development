<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StaticPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		//pr($data);die;
        $validate = [
            'slug' => 'required',
			'title' => 'required',
			'body' => 'required'
        ];
		
		if(array_key_exists('banner',$data) && $data['banner'] == 1){
			if(isset($data['id'])){
				$validate['bannerImage'] = 'image|mimes:jpeg,jpg,bmp,png|max:204800';
			}else{
				$validate['bannerImage'] = 'required|image|mimes:jpeg,jpg,bmp,png|max:204800';
			}
		}
		
		return $validate;
    }
	
	public function messages(){
		return [];
	}
}
