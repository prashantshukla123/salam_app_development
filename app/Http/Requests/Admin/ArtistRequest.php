<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ArtistProfile;
use DB;

class ArtistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		//pr($data);die;
        $validate = [
            'name' => 'sometimes|required|string|sanitizeScripts|max:20|regex:/^[a-zA-Z\\s]+$/u',
			'dob' => 'sometimes|required',
        ];
		if(isset($data['id'])){
			$validate['email'] = 'sometimes|required|string|email|max:50|unique:users,email,'.$data['id'];
			
			$validate['username'] = 'sometimes|required|sanitizeScripts|unique:users,username,'.$data['id'];
			
			if(!empty($data['password']))
				$validate['password'] = 'sometimes|required|string|min:6';
			
			$validate['artistDetails.social_handle'] =  [
				'max:100',
				function ($attribute, $value, $fail) use ($data) {
					if(!empty($value)){
						$detail = ArtistProfile::where(['social_handle' => $value, 'networkType' => $data['artistDetails']['networkType']])->where('user_id', '!=', $data['id'])->first();
						if(!empty($detail) && $detail->count() > 0){
							return $fail(__('This handle has already been taken.'));
						}
					}
				}
			];
		}else{
			$validate['email'] = 'sometimes|required|string|email|max:50|unique:users';
			$validate['username'] = 'sometimes|required|sanitizeScripts|unique:users';
			$validate['password'] = 'sometimes|required|string|min:6';
			$validate['image'] = 'sometimes|required|mimes:jpeg,jpg,gif,png|max:5120';
			
			$validate['artistDetails.social_handle'] =  [
				'max:100',
				function ($attribute, $value, $fail) use ($data) {
					if(!empty($value)){
						$detail = ArtistProfile::where(['social_handle' => $value, 'networkType' => $data['artistDetails']['networkType']])->first();
						if(!empty($detail) && $detail->count() > 0){
							return $fail(__('This handle has already been taken.'));
						}
					}
				}
			];
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			'name.regex' => 'Special character not allow.',
			'name.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'username.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'image.mimes' => 'Please upload a valid image.',
			'image.max' => 'The image must be less than 5 MB.',
		];
	}
}
