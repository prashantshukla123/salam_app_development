<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		//pr($data);die;
		$admin_id = \Auth::user()->id;
        $validate = [
            'name' => 'sometimes|required|string|sanitizeScripts|regex:/^[a-zA-Z\\s]+$/u',
			'email' => 'sometimes|required|string|email|max:50|unique:users,email,'.$admin_id,
		];
		
		if(isset($data['changePassword'])){
			$validate['OldPassword'] =  [
				'sometimes',
				'required',
				'min:6', 
				'regex:/[a-z]/',      // must contain at least one lowercase letter
				'regex:/[A-Z]/',      // must contain at least one uppercase letter
				'regex:/[0-9]/',      // must contain at least one digit
				'regex:/[@$!%*#?&]/',
											
				function ($attribute, $value, $fail) use ($data) {
					if (!\Hash::check($value, \Auth::user()->password)) {
						return $fail(__('The old password is incorrect.'));
					}
				}
			];
			
			$validate['password'] = [
				'sometimes',
				'required',
				'min:6',
				'regex:/[a-z]/',      // must contain at least one lowercase letter
				'regex:/[A-Z]/',      // must contain at least one uppercase letter
				'regex:/[0-9]/',      // must contain at least one digit
				'regex:/[@$!%*#?&]/',
			];
			
			$validate['confirmPassword'] = [
				'sometimes',
				'required',
				'min:6',
				'regex:/[a-z]/',      // must contain at least one lowercase letter
				'regex:/[A-Z]/',      // must contain at least one uppercase letter
				'regex:/[0-9]/',      // must contain at least one digit
				'regex:/[@$!%*#?&]/',
				'required_with:password',
				'same:password',
			];
		}
		
		return $validate;
    }
	
	public function messages(){
		return [
			'name.regex' => 'Special character not allowed.',
			'name.sanitize_scripts' => 'Script tags are not allowed for this field.',
			'OldPassword.regex' => 'Password should contain one capital letter, one special letter and one number.',
			'password.regex' => 'Password should contain one capital letter, one special letter and one number.',
			'confirmPassword.regex' => 'Password should contain one capital letter, one special letter and one number.',
		];
	}
}
