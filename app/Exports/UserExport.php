<?php 
namespace App\Exports;
use App\Models\User;
use App\Models\FanProfile;
use App\Models\ArtistProfile;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserExport implements FromQuery, WithHeadings, WithMapping
{    
    /**
    * @var Invoice $invoice
    */
	
	public function headings(): array
    {
        return [
            '#',
            'Name',            
			'Email',
			'Username',
			'DoB',
			'Network',
			'Social Handle',
			'Followers #',
			'Booking Rate',
			'Role',						
			'Status',
            'Created at',
            'Updated at'
        ];
    }
	
	public function map($user): array
    {//pr($user->fanDetails);die;
		return [
            $user->id,
            $user->name,
            $user->email,
			$user->username,
            (isset($user->fanDetails) && !empty($user->fanDetails)) ? $user->fanDetails->dob : '-',
			(isset($user->artistDetails) && !empty($user->artistDetails)) ? $user->artistDetails->networkType : '-',
			(isset($user->artistDetails) && !empty($user->artistDetails)) ? $user->artistDetails->social_handle : '-',
			(isset($user->artistDetails) && !empty($user->artistDetails)) ? $user->artistDetails->followers_count : '-',
			(isset($user->artistDetails) && !empty($user->artistDetails)) ? $user->artistDetails->booking_rate : '-',
			($user->role_id=='2')? "Fan":"Artist", 
			($user->status=='1')? "Active":"Inactive", 
			$user->created_at,
			$user->updated_at           
        ];
    }
	
    public function query()
    {
        return User::query()->with([
			'fanDetails' => function($query){
				$query->select(['user_id','dob']);
			},
			'artistDetails' => function($query){
				$query->select(['user_id','networkType', 'social_handle', 'followers_count', 'booking_rate']);
			}])
			->select('id','name', 'email', 'username', 'role_id', "status", 'created_at','updated_at')
			->where('id', '>', 1);
    }
}
