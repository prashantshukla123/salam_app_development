<?php

function getChildIds($ids, $data){
	if(!empty($data) && $data->count() > 0){
		foreach($data as $cat){
			$ids[] = $cat->id;
			
			if(!empty($cat->children) && count($cat->children) > 0)
				$ids = getChildIds($ids,$cat->children);
		}
		return $ids;
	}
}

/** Below code for make array of parent child hierarchy  **/
function childData($data, $escpCat = 0,$modules, $dotStr = ''){
	if(!empty($data)){
		foreach($data as $val){
			if($escpCat != $val->id){
				$modules[$val->id] = $dotStr.$val->category_name;
				if($val->children && $val->children->count() > 0){
					$modules = childData($val->children, $escpCat, $modules, $dotStr.'---');
				}
			}
		}
		return $modules;
	}
}

function getCatChildRow($data, $str = ''){
	if(!empty($data) && count($data) > 0){
		$i = 0;
		foreach($data as $k=>$v){
			$str .= '<tr class="child child_'. $v->parent_id .'" style="display:none;">';
				$str .= '<td>';
					//$str .= ++$i;
					
					if(!empty($v->children) && count($v->children) > 0){
						$str .= '<button class="openCloseRow openBtn" data-id="'. $v->id .'" data-type="campaign"><i class="fa fa-plus" aria-hidden="true"></i></button>';
					}
					
				$str .= '</td>';
				
				$str .= '<td>'. $v->category_name .'</td>';
				$str .= '<td class="project-state">';
					if($v->status==1)
						$str .= '<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Deactivate Category" title="Deactivate Category" rel="deactivate" data-id="'. $v->id .'" data-url="'. url("sitecontrol/categories/updatestatus/") .'" data-target="#StatusBox"><span class="badge badge-success">Active</span></a>';
					else
						$str .= '<a href="javascript:void(0)" class="btn label label-success RecordUpdateClass" data-toggle="modal" data-original-title="Activate Category" title="Activate Category" rel="activate" data-id="'. $v->id .'" data-url="'. url("sitecontrol/categories/updatestatus/") .'" data-target="#StatusBox"><span class="badge badge-danger">Deactive</span></a>';
					
				$str .= '</td>';
				
				$str .= '<td class="project-actions text-right">';
					$str .= '<a href="'.url('/sitecontrol/categories/edit/'.$v->id).'" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Edit Detail" title="Edit Detail"><i class="fa fa-pencil-alt"></i></a>';
					$str .= '&nbsp;';
					$str .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger RecordDeleteClass" data-toggle="modal" data-original-title="Delete category" title="Delete category" rel="'. $v->id .'" data-target="#deleteBox" data-url="'. url("sitecontrol/categories/delete/") .'"><i class="fa fa-trash"></i></a>';
				$str .= '</td>';
			
			$str .= '</tr>';
			
			if(!empty($v->children) && count($v->children) > 0){
				$str = getCatChildRow($v->children, $str);
			}
		}
		return $str;
	}
}

?>